<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\deliveryMan;
use App\Models\Logistics;
use App\Models\Wallet;


use DB;
use Config;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Carbon\Carbon;
use Rave;


class LogisticsController extends Controller
{
   
    public function __construct() {
       
        $this->deliveryMan = new deliveryMan;
    }
    
    public function index()
    {
        //dd(Hash::make('12345678'));
    }
    
    
    public function authenticate(Request $request)
        {
            
            $email=$request->username;
            $password=$request->password;
            
            if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Authentication passed...
            
         $deliveryMan = deliveryMan::SELECT('*')
       ->where('email','=',$email) 
       ->orderby('id', 'DESC')->first();
       
             return response()->json(['message'=>'Login Successful','deliveryMan' => $deliveryMan]);
        }else{
            return response()->json(['message'=>'Login Fail']);
        }
            
        }
        
       public function getAllLogistics(){
       $Logistics = Logistics::SELECT('*')
       ->orderby('id', 'asc')->get();
        return response()->json(['AllLogistics'=>$Logistics]);    
    }

    public function getAllPendingLogistics(){
       $Logistics = Logistics::SELECT('*')
       ->where('pending','=','1') 
       ->orderby('id', 'asc')->get();
        return response()->json(['AllLogistics'=>$Logistics]);    
    }

    public function ViewSingleLogistics($id){
        $Logistics = Logistics::SELECT('*')
       ->where('id','=',$id) 
       ->orderby('id', 'asc')->get();
       
      
        return response()->json($Logistics);
    }

    public function pickALogistics(Request $request){
        //get id of a logistic and update the delivery man id against the 
        $logistic_id=$request->input('logistics_id');
        $delivery_man_id=$request->input('delivery_man_id');
        
        $logistic = Logistics::find($logistic_id);
        $logistic ->pending = '0';
        $logistic ->ongoing = '1';
        $logistic ->delivery_man = $request->input('delivery_man_id');
        
        //get the delivery man wallet balance
        $wallet= Wallet::where('userid',$delivery_man_id)->orderby('id','desc')->first();
        $wallet_balance= $wallet->balance;
        
        
        if($wallet_balance > 0){
            if($logistic->save()){
                
                //charged the Delivery man wallet
                $amount=50;
                $new_balance=$wallet_balance-$amount;
                $newWallet = Wallet::create([
                    'userid' => $delivery_man_id,
                    'balance' => $new_balance,
                    'amount' => $amount
                
                ]);
                if($newWallet){
                    return response()->json(['logistic_id'=>$logistic_id ,'message'=>'Successful','new_balance'=>$new_balance]);
                }else{
                    return response()->json(['logistic_id'=>$logistic_id ,'message'=>'Something went wrong, Could not charge Wallet']);
                }
               

            }
        }else{
            return response()->json(['logistic_id'=>$logistic_id ,'message'=>'Your Wallet balance is insuficient']);
        }
       

    }
    
    public function fundwalet(Request $request){
         $delivery_man_id=$request->delivery_man_id;
         $total=$request->total;
         
          $sql = Wallet::SELECT('*')
        ->where('userid','=',$delivery_man_id) 
        ->orderby('id', 'desc')->get()->first();
        
          $balance=$sql ->balance;
          
          $amountPaid=$balance + $total;
          
          
            Wallet::create([
                'userid' =>$delivery_man_id,
                'amount' =>$total,
                'balance' =>$amountPaid,
        ]);
       
        
        return response()->json(['response'=>"Wallet Funded Successfully"]); 
          
    }

    public function ViewAllMyDeliveredLogistics(Request $request){
        $delivery_man_id=$request->delivery_man_id;
        $AllMydeliveries = Logistics::SELECT('*')
        ->where('delivered','=','1') 
        ->where('delivery_man','=',$delivery_man_id) 
        ->orderby('id', 'asc')->get();
         return response()->json(['AllMydeliveries'=>$AllMydeliveries]);    
    }

    public function ViewAllMyOnGoingLogistics(Request $request){
        $delivery_man_id=$request->delivery_man_id;
        $AllMyOngoingdeliveries = Logistics::SELECT('*')
        ->where('ongoing','=','1') 
        ->where('delivery_man','=',$delivery_man_id) 
        ->orderby('id', 'asc')->get();
         return response()->json(['AllMyOngoingdeliveries'=>$AllMyOngoingdeliveries]); 
    }
    
     public function ViewMyRejectedLogistics(Request $request){
        $delivery_man_id=$request->delivery_man_id;
        $AllMyRejecteddeliveries = Logistics::SELECT('*')
        ->where('rejected','=','1') 
        ->where('delivery_man','=',$delivery_man_id) 
        ->orderby('id', 'asc')->get();
         return response()->json(['AllMyRejecteddeliveries'=>$AllMyRejecteddeliveries]); 
    }
    
      public function ViewMyCanceledLogistics(Request $request){
        $delivery_man_id=$request->delivery_man_id;
        $AllMyCanceleddeliveries = Logistics::SELECT('*')
        ->where('cancel_id','=','1') 
        ->where('delivery_man','=',$delivery_man_id) 
        ->orderby('id', 'asc')->get();
         return response()->json(['AllMyCanceleddeliveries'=>$AllMyCanceleddeliveries]); 
    }
    
public function SendDeliveryCode(Request $request){
        $logistic_id=$request->input('logistics_id');
        
        $logistic = Logistics::find($logistic_id);
        
        $to=$logistic->email;
        $name=$logistic->name;
        
        $delivery_code=$logistic->delivery_code;
        
        
$from = 'eatnusyn@business89.web-hosting.com'; 
$fromName = 'EatNaija.com'; 
 
$subject = "EatNaija Logistics Services"; 
 
$htmlContent = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Demystifying Email Design</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<style type="text/css">
a[x-apple-data-detectors] {color: inherit !important;}
</style>

</head>
<body style="margin: 0; padding: 0;">
<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td style="padding: 20px 0 30px 0;">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; border: 1px solid #cccccc;">
<tr>
<td align="center" bgcolor="#fd7e14" style="padding: 40px 0 30px 0;">
<img src="https://eatnaija.com/public/img/logo-white.png" alt="EatNaija Logo." width="200" height="150" style="display: block;" />
</td>
</tr>
<tr>
<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
<tr>
<td style="color: #153643; font-family: Arial, sans-serif;">
<h1 style="font-size: 24px; margin: 0;">Hello '.$name.'!</h1>
<h3>EatNaija Logistics Services</h3>
</td>
</tr>
<tr>
<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;">
<p style="margin: 0;">Your EatNaija Logistics Services Code is '.$delivery_code.'</p>
<p>We lead you to places, reveal dishes we think you will like to visit and taste</p>
<p>Thanks for choosing EatNaija</p>
</td>
</tr>
<tr>
<td>

</td>
</tr>
<tr>
<td bgcolor="#fd7e14" style="padding: 30px 30px;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
    <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
<p style="margin: 0;">©2020 EatNaija</p>
</td>
<tr>

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</body>
</html>'; 
 
// Set content-type header for sending HTML email 
$headers = "MIME-Version: 1.0" . "\r\n"; 
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
 
// Additional headers 
$headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n"; 

// Send email 
   $send = mail($to, $subject, $htmlContent, $headers);
   
        return response()->json(['emailResponse'=>$send,'response'=>'Success', 'delivery_code'=>$delivery_code]); 
        
    } 

public function confirmdelivery(Request $request){
        $logistic_id=$request->input('logistics_id');
        
        $logistic = Logistics::find($logistic_id);
        $logistic ->delivered = '1';
        $logistic ->ongoing = '0';
        $logistic ->pending = '0';
        $logistic ->save();
        
         if($logistic){
            return response()->json(['response'=>"Success"]); 
        }
}

public function canceledelivery(Request $request){
        $logistic_id=$request->input('logistics_id');
        
        $logistic = Logistics::find($logistic_id);
        $logistic ->cancel_id = '0';
     
        $logistic ->ongoing = '0';
        $logistic ->pending = '1';
        
        $logistic ->save();
        
         if($logistic){
            return response()->json(['response'=>"Success"]); 
        }
}

public function checkBalance(Request $request){
    $delivery_man_id=$request->input('delivery_man_id');
     $Wallet = Wallet::SELECT('*')
       ->where('userid','=',$delivery_man_id) 
       ->orderby('id', 'DESC')->first();
      return response()->json($Wallet);  
}

}