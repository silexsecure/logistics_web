<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Data;
use DB;
class DataController extends Controller
{
  
    public function getdistance(Request $request,$unit = 'k')
{
  $apiKey = 'AIzaSyBZWSNk4j_3B_35MlhMvd0Nz94GJEtUa4U';
  $depature=$request->depart; 
  $currentaddress =$request->currentadd;

  $addressFrom = $depature;
  $addressTo = $currentaddress;

 
  // Change address format
  $formattedAddrFrom    = str_replace(' ', '+', $addressFrom);
  $formattedAddrTo     = str_replace(' ', '+', $addressTo);
  
  // Geocoding API request with start address
  $geocodeFrom = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($formattedAddrFrom).'&sensor=false&key='.$apiKey);
  $outputFrom = json_decode($geocodeFrom);
  if(!empty($outputFrom->error_message)){
      return $outputFrom->error_message;
  }
  
  // Geocoding API request with end address
  $geocodeTo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($formattedAddrTo).'&sensor=false&key='.$apiKey);
  $outputTo = json_decode($geocodeTo);
  if(!empty($outputTo->error_message)){
      return $outputTo->error_message;
  }
  
  // Get latitude and longitude from the geodata
  $latitudeFrom    = $outputFrom->results[0]->geometry->location->lat;
  $longitudeFrom    = $outputFrom->results[0]->geometry->location->lng;
  $latitudeTo        = $outputTo->results[0]->geometry->location->lat;
  $longitudeTo    = $outputTo->results[0]->geometry->location->lng;
  
  // Calculate distance between latitude and longitude
  $theta    = $longitudeFrom - $longitudeTo;
  $dist    = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
  $dist    = acos($dist);
  $dist    = rad2deg($dist);
  $miles    = $dist * 60 * 1.1515;
  
  // Convert unit and return distance
  $unit = strtoupper($unit);
  if($unit == "K"){
      $dist= round($miles * 1.609344, 2);
  }
  if($dist <= "10"){
    $distance = $dist*65.5;
}
if($dist >= "11"){
    $distance = $dist*100.6;
}
$pend=1;
$order = new Data;
$order->name = $request -> input('name');
$order ->phone =$request -> input('phone');
$order ->email =$request -> input('email');
 $order ->pickup =$request -> input('pickup');
 $order ->payment =$request -> input('payment');
 $order->depart = $addressFrom;
 $order->currentadd = $addressTo;
 $order->cost=$distance;
 $order->pending=$pend;
 $order -> save();

 return view('index')->with('distance',$distance)->with('addressFrom',$addressFrom)->with('addressTo',$addressTo)->with('order',$order);

  }

  public function pay(Request $request,$id)
  {
    $status =Data::find($id);
    $status->payment = 1;
    $status-> save();

   return redirect('index')->with('success','Order received, we will get back to you shortly');
  
    }
  public function vin_search(Request $request){
      $apiPrefix = "https://api.vindecoder.eu/3.1";
    $apikey = "4715ae606048";   // Your API key
    $secretkey = "710f1e552a";  // Your secret key
    $id = "decode";
     $vn = "JA4LZ41G63U089344";

    if(empty($vn)){
        return response() -> json(['error'=>"Please Provide VIN Number"]);
    }
    $vin = mb_strtoupper($vn);
    $controlsum = substr(sha1("{$vin}|{$id}|{$apikey}|{$secretkey}"), 0, 10);
    
    $data = file_get_contents("{$apiPrefix}/{$apikey}/{$controlsum}/decode/{$vin}.json", false);
    $result = json_decode($data); 
    return response()->json(['result'=>$result, 'vin'=>$vn]);   
  }
    

}