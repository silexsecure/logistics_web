<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Admin;
use App\Delivery;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:admin')->except('logout');

    }




    public function showAdminLoginForm()
    {
        return view('auth.adminlogin',['url' => 'admin']);
    }
    
    public function showAdminLoginResetForm()
    {
        return view('auth.resetpassword',['url' => 'reset']);
    }

    public function adminLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect('/dashboard');
                }
                return redirect('/login/admin')->with('error', 'Your credentials do not match our records');
      
    }

    public function showDeliveryLoginForm()
    {
        return view('auth.deliverylogin',['url' => 'delivery']);
    }

    public function deliveryLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:8'
        ]);

        if (Auth::guard('deliver')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect('/dashboard2');
                }
                return redirect('/login/delivery')->with('error', 'Your credentials do not match our records');
      
    }


}
