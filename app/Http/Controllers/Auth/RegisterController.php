<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Admin;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Delivery;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('guest:admin');

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }


    public function showAdminRegisterForm()
    {
        return view('auth.adminregister', ['url' => 'admin']);
    }

  


    protected function createAdmin(Request $request)
    {
        $this->validate($request, [
            'name' => 'required', 'string', 'max:255',
            'email' => 'required', 'string', 'email', 'max:255', 'unique:admin',
            'password' => 'required', 'string', 'min:8', 'confirmed',
        ]);
        if(isset($request['image'])){
            //get file name and extension
            $filenamewithext=$request['image']->getClientOriginalName();
            //get just file name
            $filename = pathinfo($filenamewithext, PATHINFO_FILENAME);
            //get just extension
            $extension = $request['image'] -> getClientOriginalExtension();
            // filename to store
            $filenametostore= $filename. '_'.time().'.'.$extension;
            //upload image
            $path =$request['image']->storeAs('public/admin_image', $filenametostore);
        } else{
            $filenametostore='noimage.jpg';
        }
        
        $admin = Admin::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'password' => Hash::make($request['password']),
            'image'=>$filenametostore,
        ]);
        return redirect()->intended('login/admin');
    }


    public function showdeliveryRegisterForm()
    {
        return view('delivery-men.adddelivery');
    }

  


    protected function createdelivery(Request $request)
    {
        $this->validate($request, [
            'name' => 'required', 'string', 'max:255',
            'phone' => 'required', 'string', 'max:255',
            'state' => 'required', 'string', 'max:255',
            'email' => 'required', 'string', 'email', 'max:255', 'unique:delivery_men',
            'password' => 'required', 'string', 'min:8', 'confirmed',
        ]);
        if(isset($request['image'])){
            //get file name and extension
            $filenamewithext=$request['image']->getClientOriginalName();
            //get just file name
            $filename = pathinfo($filenamewithext, PATHINFO_FILENAME);
            //get just extension
            $extension = $request['image'] -> getClientOriginalExtension();
            // filename to store
            $filenametostore= $filename. '_'.time().'.'.$extension;
            //upload image
            $path =$request['image']->storeAs('public/delivery_image', $filenametostore);
        } else{
            $filenametostore='noimage.jpg';
        }
        
        $admin = Delivery::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'state' => $request['state'],
            'password' => Hash::make($request['password']),
            'image'=>$filenametostore,
            'company_id'=> Auth::guard('admin')->user()->id,
        ]);
        return redirect('login/delivery');
    }


}
