<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Data;
use Auth;
use Carbon\Carbon;
use App\Wallet;
use App\Delivery;
use DB;
class DeliveryController extends Controller
{
    public function index()
    {
       
        $user=Auth::guard('deliver')->user();
        if(!$user){
       return redirect('login/delivery');
        }
        $pending=Data::where('pending','1')->get();
        $ongoing=Data::where('ongoing','1')->where('delivery_man',$user->id)->get();
        $delivered=Data::where('delivered','1')->where('delivery_man',$user->id)->get();
        $rejected=Data::where('cancel_id',$user->id)->get();

        $orders_last=Data::where('delivered','1')->where('delivery_man',$user->id)->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->get();
        $orders_present=Data::where('delivered','1')->where('delivery_man',$user->id)->where('created_at', '>=', Carbon::now()->startOfMonth()->toDateTimeString())->get();
        $orders_today=Data::where('delivered','1')->where('delivery_man',$user->id)->where('created_at', '>=',  Carbon::now()->subDays(1))->get();


        $total=Data::where('delivered','1')->where('delivery_man',$user->id)->sum('cost');
        $last=Data::where('delivered','1')->where('delivery_man',$user->id)->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->sum('cost');
        $present=Data::where('delivered','1')->where('delivery_man',$user->id)->where('created_at', '>=', Carbon::now()->startOfMonth()->toDateTimeString())->sum('cost');
        $today=Data::where('delivered','1')->where('delivery_man',$user->id)->where('created_at', '>=',  Carbon::now()->subDays(1))->sum('cost');
$wallet=Wallet::where('userid',$user->id)->orderby('id','desc')->first();
if(!$wallet){
    $balance=0;
}else{
$balance=$wallet->balance;
}
  return view('dashboard2',compact('pending','ongoing','delivered','rejected','total','last','present','today','orders_today','orders_present','orders_last','balance'));

    }



    public function pending(request $request)
    {       
        $user=Auth::guard('deliver')->user();
        if(!$user){
       return redirect('login/delivery');
        }
        $data = Data::where('pending','1')->orderby('id', 'desc')->paginate(10);
       
        if ($request->ajax()) {
            return response()->json($data);
        }
        return view ('delivery-men.pending',compact('data'));
    }

    public function pendingAjax(request $request)
    {       
        $user=Auth::guard('deliver')->user();
        if(!$user){
       return redirect('login/delivery');
        }
        $pendings = DB::table('logistics')->latest()->first();
        return response()->json(['pendings'=>$pendings]);
    }
    public function ongoing(request $request)
    {       
        $user=Auth::guard('deliver')->user();
        if(!$user){
       return redirect('login/delivery');
        }
       
        $ongoings = Data::where('ongoing','1')->where('delivery_man',$user->id)->orderby('id', 'desc')->paginate(10);
        return view ('delivery-men.ongoing')->with('ongoings', $ongoings);
    }

    public function delivered(request $request)
    {    
        $user=Auth::guard('deliver')->user();
        if(!$user){
       return redirect('login/delivery');
        } 
        $deliveries = Data::where('delivered','1')->where('delivery_man',$user->id)->orderby('id', 'desc')->paginate(10);
        return view ('delivery-men.delivered')->with('deliveries', $deliveries);
    }

    public function rejected(request $request)
    {       
        $user=Auth::guard('deliver')->user();
        if(!$user){
       return redirect('login/delivery');
        }
        $rejecteds = Data::where('cancel_id',$user->id)->orderby('id', 'desc')->paginate(10);
        return view ('delivery-men.rejected')->with('rejecteds', $rejecteds);
    }



    public function viewOrder(Request $request, $id)
    {
        $user=Auth::guard('deliver')->user();
        if(!$user){
       return redirect('login/delivery');
        }
        $name=Auth::guard('deliver')->user()->name;
        $pending =Data::find($id);
       
         if($pending->payment==1)
         {
             $status="Pay on Delivery";
            }else{
                $status="Paid";
            }
          return view('delivery-men.receipt')->with('success', 'Order Accepted')->with('pending',$pending)->with('status',$status);
    }


    public function viewReceipt(Request $request, $id)
    {
        $user=Auth::guard('deliver')->user();
        if(!$user){
       return redirect('login/delivery');
        }
        $name=Auth::guard('deliver')->user()->name;
        $pending =Data::find($id);
       
         if($pending->payment==1)
         {
             $status="Pay on Delivery";
            }else{
                $status="Paid";
            }
          return view('delivery-men.receipt2')->with('success', 'Order Accepted')->with('pending',$pending)->with('status',$status);
    }


    public function approve($id)
    {
        $namee=Auth::guard('deliver')->user()->name;
        $name=Auth::guard('deliver')->user();
        $wallet=Wallet::where('userid',$name->id)->orderby('id','desc')->first();
        if(!$wallet){
            return response()->json(['status'=>'error', 'message'=>'Please Fund Your Wallet']);
        }elseif(50 >= $wallet->balance){
            return response()->json(['status'=>'error', 'message'=>'Your wallet balance is less than 50, Please Fund Your Wallet']);
        }elseif(50 < $wallet->balance){
        $pending =Data::find($id);
        if($pending->cancel_id==$name->id){
            return response()->json(['status'=>'error', 'message'=>'You are no longer allowed to view this order']); 
        }
         Wallet::create([
             'amount'=>50,
             'balance'=>$wallet->balance - 50,
             'userid'=>$name->id,
         ]);
        $pending->pending = NULL;
        $pending->ongoing = 1;
        $pending->delivery_man= $namee;
        $pending->delivery_man= $name->id;
        $pending->company_id=$name->company_id;
         $pending -> save();
         if($pending->payment==1)
         {
             $status="Pay on Delivery";
            }else{
                $status="Paid";
            }
            return response()->json(['status'=>'success', 'message'=>' Approved']); 

        }
    }

    public function done(Request $request, $id)
    {


       $code = $request->code;
        $deliveries =Data::find($id);
        if($code!==$deliveries->delivery_code){

            return response()->json(['status'=>'error', 'message'=>' Invalid Delivery Code']); 

        }else{
        $deliveries->pending = NULL;
        $deliveries->ongoing = NULL;
        $deliveries->delivered = 1;

         $deliveries -> save();
         $user=Auth::guard('deliver')->user();
         $total=Data::where('delivered','1')->where('delivery_man',$user->id)->sum('cost');
         $last=Data::where('delivered','1')->where('delivery_man',$user->id)->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->sum('cost');
         $present=Data::where('delivered','1')->where('delivery_man',$user->id)->where('created_at', '>=', Carbon::now()->startOfMonth()->toDateTimeString())->sum('cost');
         $today=Data::where('delivered','1')->where('delivery_man',$user->id)->where('created_at', '>=',  Carbon::now()->subDays(1))->sum('cost');
         $update=Delivery::find($user->id);
       $update->total_amount= $total;
       $update->last_month= $last;
       $update->this_month= $present;
       $update->today= $today;
       $update->save();
         return response()->json(['status'=>'success', 'message'=>' Order Completed']); 

    }
}

    public function reject(Request $request, $id)
    {
        $name=Auth::guard('deliver')->user()->name;
        $pending =Data::find($id);
        $pending->pending = NULL;
        $pending->ongoing = NULL;
        $pending->delivered = NULL;
        $pending->rejected = 1;
        $pending->delivery_man= $name;



         $pending -> save();
          return redirect('rejected2')->with('success', 'Order Rejected');
    }


    public function cancel(Request $request, $id)
    {
        $name=Auth::guard('deliver')->user();
        $wallet=Wallet::where('userid',$name->id)->orderby('id','desc')->first();
        $deliveries =Data::find($id);
        $deliveries->pending = 1;
        $deliveries->ongoing = NULL;
        $deliveries->delivered = NULL;
        $deliveries->delivery_man= NULL;
        $deliveries->delivery_man= NULL;
        $deliveries->cancel_id=$name->id;
        $deliveries->company_id=NULL;


         $deliveries -> save();
         Wallet::create([
            'amount'=>50,
            'balance'=>$wallet->balance - 50,
            'userid'=>$name->id,
        ]);
          return response()->json(['status'=>'success', 'message'=>'Order canceled']);
    }


    public function updateshowD($id)
    {
        $user=Auth::guard('deliver')->user();
        if(!$user){
       return redirect('login/delivery');
        }
        $delivery= Delivery::find($id);
        
        return view('delivery-man.editdelivery')->with('delivery', $delivery);

    }

    public function updateD(Request $request, $id)
    {
       
        $delivery =Delivery::find($id);
        $delivery->fullname = $request -> input('name');
        $delivery->email = $request -> input('email');
        $delivery->phone =$request -> input('phone');
        $delivery->state =$request -> input('state');
        $delivery->password =Hash::make($request['password']);

         $delivery-> save();
 
          return redirect($id.'editdelivery')->with('success', 'Profile Updated');
    }
    
    public function withdrawal(request $request)
    {       
      $user=Auth::guard('deliver')->user();
        if(!$user){
       return redirect('login/delivery');
        }
  $data = DB::table("driver_trans")
  ->where(["type"=>"withdraw","userid"=>$user->id])
  ->orderBy("driver_trans.id","DESC")
 ->join("delivery_men","delivery_men.id","=","driver_trans.userid")->select("driver_trans.*","delivery_men.name")->simplePaginate(9);
 
        return view ('withdrawal',compact("data"));
    }

   public function withdrawnow(Request $request){
                $user = DB::table("delivery_men")->where(["id"=>$request->delivery_man_id])->first();
                if($user->account_number == null){
                    return response()->json(["message"=>"Please add account to withdraw"],400);
                }
                if(!$user){
                    return response()->json(["message"=>"User Not Found!"],404);
                }
                $amount = $request->amount;
                $id = rand();
                 
                    
                    if($amount  > $user->wallet){
                        return response()->json(["message"=>"insufficient fund"],400);
                    }
                    
                    DB::table("driver_trans")->insert(["userid"=>$user->id,"amount"=>$amount,"trans_id"=>$id,"method"=>"transfer","type"=>"withdraw","status"=>"pending"]);
                    DB::table("delivery_men")->where(["id"=>$user->id])->decrement("wallet",$amount);
                    return response()->json(["message"=>"Withdrawal SuccessFul, Pending For Approval"]);
                    
            }
            
            public function validateacct(Request $request){
                $account_number = $request->account_number;
                $account_bank = $request->account_bank;
               
               $res =  \Http::withToken("FLWSECK_TEST-287ebe764d6c3eb9b8138796f61dfaf0-X")->post('https://api.flutterwave.com/v3/accounts/resolve',[
                    
                    "account_number"=>$account_number,
                    "account_bank"=>$account_bank
                    ]);
                    $obj = $res->object();
                    
            
                if($res->failed()){
                    return response()->json(["message"=>"Something went wrong","error"=>$obj->message],400);
                }
                return response()->json(["name"=>$obj->data->account_name]);
            }
            
            public function update_account(Request $request){
                $account_number = $request->account_number;
                $account_bank = $request->account_bank;
                   $account_name = $request->account_name; 
                   
                DB::table("delivery_men")->where(["id"=>Auth::guard('deliver')->user()->id])->update(["account_number"=>$account_number,"account_bank"=>$account_bank,"account_name"=>$account_name]);
                return response()->json(["message"=>"Updated!"]);
            }



}
