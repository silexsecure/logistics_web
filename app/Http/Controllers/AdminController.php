<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Data;
use App\Delivery;
use App\amountperkms;
use App\Admin;
use App\Wallet;
use Auth;
use DB;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function index()
    {
    
        $user=Auth::guard('admin')->user();
        if(!$user){
       return redirect('login/admin');
        }

            $pending=Data::where('pending','1')->get();
            $ongoing=Data::where('ongoing','1')->get();
            $delivered=Data::where('delivered','1')->where('company_id',$user->id)->get();
            $rejected=Data::where('company_id',$user->id)->get();

            $men=Delivery::get();

            $orders_last=Data::where('delivered','1')->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->get();
            $orders_present=Data::where('delivered','1')->where('created_at', '>=', Carbon::now()->startOfMonth()->toDateTimeString())->get();
            $orders_today=Data::where('delivered','1')->where('created_at', '>=',  Carbon::now()->subDays(1))->get();
    
    
            $total=Data::where('delivered','1')->sum('cost');
            $last=Data::where('delivered','1')->where('company_id',$user->id)->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->sum('cost');
            $present=Data::where('delivered','1')->where('created_at', '>=', Carbon::now()->startOfMonth()->toDateTimeString())->sum('cost');
            $today=Data::where('delivered','1')->where('created_at', '>=',  Carbon::now()->subDays(1))->sum('cost');
            return view('dashboard',compact('pending','ongoing','delivered','rejected','total','last','present','today','orders_today','orders_present','orders_last','men'));

    }
    
    public function settings(){
         $amountperkm=amountperkms::get()->first();
        
         return view('Admin.settings')->with('amountperkm',$amountperkm);
    }

    
    public function updateshow($id)
    {
        $user=Auth::guard('admin')->user();
        if(!$user){
       return redirect('login/admin');
        }
        $admin= Admin::find($id);
        
        return view('Admin.editadmin')->with('admin', $admin);

    }
    public function update(Request $request, $id)
    {
       
        $admin =Admin::find($id);
        $admin->name = $request -> input('name');
        $admin->email = $request -> input('email');
        $admin ->password =Hash::make($request['password']);

         $admin -> save();
 
          return redirect($id.'editadmin')->with('success', 'Profile Updated');
    }
    

    public function saveLogisticPrice(Request $request){
         $p= $request -> input('amountperkm');
         $amountperkm=amountperkms::get()->first();
         $amountperkm -> price=$p;
         $amountperkm -> save();
         
         return redirect('settings')->with('success', 'Updated successfully');
    }
    
    
    public function show(request $request)
    {       
        $user=Auth::guard('admin')->user();
        if(!$user){
       return redirect('login/admin');
        }
        $deliveries = Delivery::where('company_id',$user->id)->orderby('id', 'desc')->paginate(10);
        return view ('Admin.viewdelivery-men')->with('deliveries', $deliveries);
    }
    public function destroy($id)
    {
        $deliveries= Delivery::find($id);
        $deliveries->delete();
        return redirect('delivery-men')->with('success', 'Delivery Man Deleted');

    }

    public function updateshowD($id)
    {
        $user=Auth::guard('admin')->user();
        if(!$user){
       return redirect('login/admin');
        }
        $delivery= Delivery::find($id);
          $data = DB::table("orders")->where(["delivery_id"=>$id])->orderby('id', 'desc')->paginate(10);
        return view('Admin.editdelivery',compact("data"))->with('delivery', $delivery);

    }

    public function updateD(Request $request, $id)
    {
        $delivery =Delivery::find($id);
        $delivery->name = $request -> input('fullname');
        $delivery->email = $request -> input('email');
        $delivery->phone =$request -> input('phone');
        $delivery->state =$request -> input('state');
        $delivery->password =Hash::make($request['password']);

         $delivery-> save();
 
          return redirect($id.'editdelivery')->with('success', 'Profile Updated');
    }


    public function pending(request $request)
    {       
        $user=Auth::guard('admin')->user();
        if(!$user){
       return redirect('login/admin');
        }
        $pendings = Data::where('pending','1')->orderby('id', 'desc')->paginate(10);
        return view ('Admin.pending')->with('pendings', $pendings);
    }

    public function ongoing(request $request)
    {       
        $user=Auth::guard('admin')->user();
        if(!$user){
       return redirect('login/admin');
        }
        $ongoings = Data::where('ongoing','1')->orderby('id', 'desc')->paginate(10);
        return view ('Admin.ongoing')->with('ongoings', $ongoings);
    }

    public function delivered(request $request)
    {       
        $user=Auth::guard('admin')->user();
        if(!$user){
       return redirect('login/admin');
        }
        $deliveries = Data::where('delivered','1')->orderby('id', 'desc')->paginate(10);
        return view ('Admin.delivered')->with('deliveries', $deliveries);
    }

    public function rejected(request $request)
    {       
        $user=Auth::guard('admin')->user();
        if(!$user){
       return redirect('login/admin');
        }
        $rejecteds = Data::where('rejected','1')->orderby('id', 'desc')->paginate(10);
        return view ('Admin.rejected')->with('rejecteds', $rejecteds);
    }
    
    
       public function transactions(request $request)
    {       
        $user=Auth::guard('admin')->user();
        if(!$user){
       return redirect('login/admin');
        }
 
        return view ('Admin.transactions');
    }
    
    
    public function trans(request $request)
    {       
        $user=Auth::guard('admin')->user();
        if(!$user){
       return redirect('login/admin');
        }
 $data = DB::table("driver_trans")
 ->join("delivery_men","delivery_men.id","=","driver_trans.userid")->select("driver_trans.*","delivery_men.name")->simplePaginate(9);
        return view ('Admin.trans',compact("data"));
    }
    
    public function withdrawal(request $request)
    {       
        $user=Auth::guard('admin')->user();
        if(!$user){
       return redirect('login/admin');
        }
  $data = DB::table("driver_trans")
  ->where(["type"=>"withdraw"])
 ->join("delivery_men","delivery_men.id","=","driver_trans.userid")->select("driver_trans.*","delivery_men.name")->simplePaginate(9);
 
        return view ('Admin.withdrawal',compact("data"));
    }
  
  
  public function withdraw(Request $request){
      $id = $request->id;
      $data = DB::table("driver_trans")
      ->where(["driver_trans.id"=>$id])
 ->join("delivery_men","delivery_men.id","=","driver_trans.userid")->select("driver_trans.*","delivery_men.name","delivery_men.account_number","delivery_men.account_bank")->first();


 
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'https://api.flutterwave.com/v3/transfers');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n    \"account_bank\": \"$data->account_bank\",\n    \"account_number\": \"$data->account_number\",\n    \"amount\": $data->amount,\n    \"narration\": \"Payout to $data->name\",\n    \"currency\": \"NGN\",\n    \"reference\": \"$data->trans_id\",\n    \"callback_url\": \"\",\n    \"debit_currency\": \"NGN\"\n}");

$headers = array();
$headers[] = 'Content-Type: application/json';
$headers[] = 'Authorization: Bearer FLWSECK_TEST-a4b2aacbe5b769789f88212d1a2b050a-X';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close($ch);
$res = json_decode($result);
 
      
  
                if($res->status == "error"){
                    return response()->json(["message"=>"Something went wrong","error"=>$res->message],400);
                }
                
              DB::table("driver_trans")->where(["id"=>$id])->update(["status"=>"approved"]);
              return response()->json(["message"=>"Approved!","error"=>$res->message]);
  }
  
  public function disapprovew(Request $request){
      $id = $request->id;
      $reason = $request->reason;
           DB::table("driver_trans")->where(["id"=>$id])->update(["status"=>"disapproved","reason"=>$reason]);
           return response()->json(["message"=>"Disapproved!"]);
  }
}
