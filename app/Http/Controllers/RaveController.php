<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Rave;
use App\Data;
use App\Wallet;
use Auth;
class RaveController extends Controller
{
    /**
   * Initialize Rave payment process
   * @return void
   */public function initialize($id){
    //This initializes payment and redirects to the payment gateway//The initialize method takes the parameter of the redirect URL
    Rave::initialize(route('callback'));
  }
/**
   * Obtain Rave callback information
   * @return void
   */public function callback(Request $request){
$data = Rave::verifyTransaction(request()->txref);

}

public function verify(Request $request)
{
  
  $userid=$request->userid;
  $amount=$request->amount;
  $bal= Wallet::where('userid',$userid)->orderby('id','desc')->first();
  if(!$bal){
      $balance=0;
  }else{
  $balance = $bal->balance;
  }
  Wallet::create([
      'userid'=>$userid,
      'amount'=>$amount,
      'balance' => $amount + $balance,
  ]);
  return response()->json(['success'=>'Payment Successful']);
}
}
