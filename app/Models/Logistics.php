<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Logistics extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'phone',
        'email' ,
        'pickup' ,
        'depart',
        'currentadd' ,
        'cost',
        'payment',
        'pending',
        'ongoing',
        'delivered',
        'rejected',
        'delivery_code',
        'delivery_man',
        'delivery_man_id',
        'company_id',
        'cancel_id',
        'updated_at',
        'created_at',
        'cancel_id',
       
        
      
];
protected $table ='logistics';
public $primarykey = 'id';
}
