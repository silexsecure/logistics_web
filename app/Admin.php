<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;


class Admin extends Model implements AuthContract 
{
    use Authenticatable;

    protected $guard = 'admins';

    protected $fillable = [
        'name', 'email','phone', 'password','image',
    ];
    protected $table = 'admin2';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
