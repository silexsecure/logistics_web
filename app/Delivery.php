<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;


class Delivery extends Model implements AuthContract 
{
    use Authenticatable;

    protected $guard = 'deliver';

    protected $fillable = [
        'name', 'email','phone','state', 'password','image','today','this_month','last_month','total','company_id'
    ];
    protected $table = 'delivery_men';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
