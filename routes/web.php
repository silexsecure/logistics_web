<?php

use Illuminate\Support\Facades\Route;
use App\Admin;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
     return view('index');
});

Route::post('/', 'DataController@getdistance');
Route::get('/vin-search', 'DataController@vin_search');

Auth::routes();
Route::get('{id}editadmin',  ['as' => 'admin.updateshow', 'uses' => 'AdminController@updateshow']);

Route::patch('edit_admin/{id}',  ['as' => 'admin.update', 'uses' => 'AdminController@update']);




Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm');
Route::get('/login/reset', 'Auth\LoginController@showAdminLoginResetForm');

Route::get('/register/admin', 'Auth\RegisterController@showAdminRegisterForm');
Route::post('/login/admin', 'Auth\LoginController@adminLogin');

Route::post('/register/admin', 'Auth\RegisterController@createAdmin');

Route::get('/dashboard', 'AdminController@index')->name('dashboard');
Route::get('/settings', 'AdminController@settings')->name('Settings');


Route::get('adddelivery', 'Auth\RegisterController@showdeliveryRegisterForm');
Route::post('adddelivery', 'Auth\RegisterController@createdelivery');
Route::get('delivery-men', 'AdminController@show');
Route::delete('delete_delivery/{id}', 'AdminController@destroy');
Route::get('{id}editdelivery',  ['as' => 'delivery.updateshow', 'uses' => 'AdminController@updateshowD']);
Route::patch('edit_delivery/{id}',  ['as' => 'delivery.update', 'uses' => 'AdminController@updateD']);
Route::get('pending','AdminController@pending');
Route::get('ongoing','AdminController@ongoing');
Route::get('delivered','AdminController@delivered');
Route::get('rejected','AdminController@rejected');

Route::get('/dashboard2', 'DeliveryController@index')->name('dashboard2');
Route::get('/login/delivery', 'Auth\LoginController@showDeliveryLoginForm');
Route::post('/login/delivery', 'Auth\LoginController@deliveryLogin');
Route::get('pending2','DeliveryController@pending');
Route::get('pendingajax','DeliveryController@pendingAjax');
Route::get('ongoing2','DeliveryController@ongoing');
Route::get('delivered2','DeliveryController@delivered');
Route::get('rejected2','DeliveryController@rejected');
Route::post('accept_order/{id}', 'DeliveryController@approve');
Route::post('delivered_order/{id}', 'DeliveryController@done');
Route::post('reject_order/{id}', 'DeliveryController@reject');
Route::post('cancel_order/{id}', 'DeliveryController@cancel');
Route::get('{id}editdeliveryD',  ['as' => 'delivery.updateshow', 'uses' => 'AdminController@updateshowD']);
Route::patch('edit_deliveryD/{id}',  ['as' => 'delivery.update', 'uses' => 'AdminController@updateD']);
Route::get('view_order{id}', 'DeliveryController@viewOrder');
Route::get('view_receipt{id}', 'DeliveryController@viewReceipt');


Route::post('updatelogistic', 'AdminController@saveLogisticPrice');
 




Route::post('/payy', 'PaymentController@redirectToGateway')->name('payy');
Route::get('/payment/callback', 'PaymentController@handleGatewayCallback');
Route::patch('payment/{id}', 'DataController@pay');

Route::post('/pay', 'RaveController@initialize');
Route::get('/rave/callback', 'RaveController@callback');
Route::get('/transactions', 'AdminController@transactions');
Route::get('/trans', 'AdminController@trans');
Route::get('/withdrawal', 'AdminController@withdrawal');
Route::post('/withdraw', 'AdminController@withdraw');
Route::post('/withdraw/disapprove', 'AdminController@disapprovew');
Route::post('/subscription', 'RaveController@verify');


Route::get('/withdrawals', 'DeliveryController@withdrawal');


Route::post('/withdrawnow', 'DeliveryController@withdrawnow');
Route::post('/validateacct', 'DeliveryController@validateacct');
Route::post('/update_account', 'DeliveryController@update_account');


