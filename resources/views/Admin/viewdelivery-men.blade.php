<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="public/assets/images/loader.svg" width="48" height="48" alt="Aero"></div>
        <p>Please wait...</p>
    </div>
</div>
@extends('layout.header')
@extends('layouts.app')
<!-- Main Content -->

<section class="content">
<div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Delivey Men</h2>
                <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i>GapaNaija Logistics</a></li>
                    <li class="breadcrumb-item active">Delivery Men</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">                
                <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
            </div>
        </div>
    </div>
            <div class="row clearfix">
                <div class="col-sm-12 col-md-12 col-lg-12">
                @if(session('success'))
                 <div class="alert alert-success">
                 <button type="button" class="close" data-dismiss="alert">×</button>
                  {{session('success')}}
                 </div>
                @endif
                <button class="btn" onclick="page()">print</button>

                     <script>
                     function page(){
                      window.print()
                        }
                        </script>
                <div class="card">
                   @if(count($deliveries)>0)
                   <div class="table-responsive">
                       <table class="table table-hover product_item_list c_table theme-color mb-0">
                           <thead>
                               <tr>
                                   <th>Image</th>
                                   <th data-breakpoints="sm xs">Fullname</th>
                                   <th data-breakpoints="xs">Email</th>
                                   <th data-breakpoints="xs md">Phone Number</th>
                                   <th data-breakpoints="xs md">State</th>
                                   <th data-breakpoints="xs md">Amount made Today</th>
                                   <th data-breakpoints="xs md">Amount Made this Moth</th>
                                   <th data-breakpoints="xs md">Amount made last Month</th>
                                   <th data-breakpoints="xs md">Total amount made</th>
                                   <th data-breakpoints="sm xs md">Joined at</th>
                                   <th data-breakpoints="sm xs md">Action</th>
                               </tr>
                           </thead>
                           <tbody>
                                @foreach($deliveries as $delivery)
                                    <tr>
                                    <td><img class="w50"  src="public/storage/delivery_image/{{$delivery->image}}"   alt="img" ></td>
                                        <td>{{$delivery->name}}</td>
                                        <td>{{$delivery->email}}</td>
                                        <td>{{$delivery->phone}}</td>
                                        <td>{{$delivery->state}}</td>
                                        <td>&#8358 {{number_format($delivery->today, 2)}}</td>
                                        <td>&#8358 {{number_format($delivery->this_month, 2)}}</td>
                                        <td>&#8358 {{number_format($delivery->last_month, 2)}}</td>
                                        <td>&#8358 {{number_format($delivery->total_amount, 2)}}</td>
                                        <td>{{$delivery->created_at->format('D-d-M-y h:i A')}}</td>
                                        <td>
                                        <a class="btn btn-default waves-effect waves-float btn-sm waves-green" href="/{{$delivery->id}}editdelivery" class="btn btn-primary"><i class="zmdi zmdi-edit"></i></a>

                                       
            {!! Form::open(['action' =>['AdminController@destroy', $delivery->id], 'method'=> 'POST'])!!}
{{Form::hidden('_method','DELETE')}}
{{Form::button('<i class="zmdi zmdi-delete"></i>',['class'=>'btn btn-danger btn btn-default waves-effect waves-float btn-sm waves-red', 'type'=>'submit'])}}
{!!Form::close()!!}
            </td>
            </tr>
            @endforeach
            </table>
            @else
            <p>You have no Delivery Men</p>
            <a href="adddelivery" class="btn btn-success">Create Delivery Man</a>
                                    </tr>
                                  
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
               
        </div>
    </div>
    <div class="card">
                        <div class="body text-center">                            
                            <ul class="pagination pagination-primary m-b-0 text-center">
                                 {{$deliveries->render()}}
                               
                            </ul>
                        </div>
                    </div>    
    @endif
</section>