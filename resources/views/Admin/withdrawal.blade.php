<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="public/assets/images/loader.svg" width="48" height="48" alt="Aero"></div>
        <p>Please wait...</p>
    </div>
</div>
@extends('layout.header')
<!-- Overlay For Sidebars -->
<div class="overlay"></div>

@extends('layouts.app')
<!-- Main Content -->


<section class="content">
<div class="container-fluid">
<div class="block-header">
            <div class="row clearfix">
       <div class="col-lg-7 col-md-6 col-sm-12">
           <h2>Withdrawal</h2>
           <ul class="breadcrumb">
           <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i>GapaNauja Logistics</a></li>
              
           </ul>
           <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
       </div>
       <div class="col-lg-5 col-md-6 col-sm-12">                
           <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
       </div>
   </div>
</div>
</div>
                <div class="col-lg-12">
                @if(session('success'))
                 <div class="alert alert-success">
                  {{session('success')}}
                 </div>
                @endif
                @if(count($data)>0)
                <button class="btn" onclick="page()">print</button>

                <script>
                function page(){
                 window.print()
                   }
                   </script>
                    <div class="card">
                        <div class="table-responsive">
                            <table class="table table-hover product_item_list c_table theme-color mb-0">
                                <thead>
                                    <tr>
                                        <th>Customer Name</th>
                                        <th>Amount</th>
                                        <th data-breakpoints="sm xs">Method</th>
                                        <th data-breakpoints="xs">Type</th>
                                        <th data-breakpoints="xs md">Transaction ID</th>
                                        <th data-breakpoints="xs md">Status</th>
                                         <th data-breakpoints="xs md">Reason</th>
                                        <th data-breakpoints="xs md">Date</th>
                                             <th data-breakpoints="xs md"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $d)
                                    <tr>
                                        <td>{{$d->name}}</td>
                                        <td>NGN{{$d->amount}}</td>
                                        <td>{{$d->method}}</td>
                                        <td>{{$d->type}}</td>
                                        <td>{{$d->trans_id}}</td>
                                        <td>{{$d->status}}</td>
                                         <td>{{$d->reason}}</td>
                                        <td>{{$d->created_at}}</td>
                                        <td>
                                          @if($d->status == "approved")
                                          --
                                          
                                          @else
                                            <div class="dropdown">
  <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action
  </a>

  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
    <a class="dropdown-item" href="#" onclick="setstatus('approve',{{$d->id}})">Approve</a>
    <a class="dropdown-item" href="#" onclick="setstatus('decline',{{$d->id}})">Decline</a> 
  </div>
</div>
                                          @endif
                                            </td>
                                    </tr>
                                   
                                     


                                    @endforeach
                                   
                    </div>
                                    </tr>        
                                </tbody>
                            </table>
                        </div>
                    </div>   
                    <div class="card">
                        <div class="body text-center">                            
                            <ul class="pagination pagination-primary m-b-0 text-center">
                                 {{$data->links()}}
                               
                            </ul>
                        </div>   
                    @else
            <p>You have no transactions</p>
            @endif          
            </div>
        </div>
    </div>
   
</section>
 <div class="modal" tabindex="-1" role="dialog" id="reason">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Reason</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form>
      <div class="modal-body">
      <div class="form-group">
          <label>Enter Reason For Disapproval</label>
          <textarea class="form-control" id="areason"></textarea>
      </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">continue</button> 
      </div>
      </form>
    </div>
  </div>
</div>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    function setstatus(status,id){
        $("#id").val(id)
        if(status == "approve"){
        swal({
  title: "Are you sure?",
  text: "",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
   
        if(status == "approve"){
            $.post('/withdraw',{
                id,
                "_token":"{{csrf_token()}}"
            }).then(function(res){
                swal(res.message,res.error,"success")
                location.reload()
            }).fail(function(res){
                    swal(res.responseJSON.message,res.responseJSON.error,"error")
            })
        }
  }  
});
}else{
    $("#reason").modal("show")
}
    }
     
       
 (function() { 
setTimeout(()=>{
     $("form").submit(function(){
         $("button").attr("disabled",true)
        event.preventDefault()
        const id = $("#id").val()
        const reason  = $("#areason").val()
         $.post('/withdraw/disapprove',{
                id,
                reason,
                "_token":"{{csrf_token()}}"
            }).done(function(res){
                $("button").attr("disabled",false)
                swal("",res.message,"success")
                location.reload()
            }).fail(function(res){
                 $("button").attr("disabled",false)
                    swal("",res.responseJSON.message,"error")
            })
    })
},3000)
})();
</script>
<input type="hidden" id="id">
