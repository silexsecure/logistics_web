<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="public/assets/images/loader.svg" width="48" height="48" alt="Aero"></div>
        <p>Please wait...</p>
    </div>
</div>
@extends('layout.header')
<!-- Overlay For Sidebars -->
<div class="overlay"></div>

@extends('layouts.app')
<!-- Main Content -->


<section class="content">
<div class="container-fluid">
<div class="block-header">
            <div class="row clearfix">
       <div class="col-lg-7 col-md-6 col-sm-12">
           <h2>Pending Orders</h2>
           <ul class="breadcrumb">
           <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i>GapaNaija Logistics</a></li>
              
           </ul>
           <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
       </div>
       <div class="col-lg-5 col-md-6 col-sm-12">                
           <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
       </div>
   </div>
</div>
</div>
                <div class="col-lg-12">
                @if(session('success'))
                 <div class="alert alert-success">
                  {{session('success')}}
                 </div>
                @endif
                @if(count($pendings)>0)
                <button class="btn" onclick="page()">print</button>

                <script>
                function page(){
                 window.print()
                   }
                   </script>
                    <div class="card">
                        <div class="table-responsive">
                            <table class="table table-hover product_item_list c_table theme-color mb-0">
                                <thead>
                                    <tr>
                                        <th>Customer Name</th>
                                        <th>Customer Phone Number</th> 
                                        <th data-breakpoints="xs">Cost</th>
                                        
                                        <th data-breakpoints="xs md">Arrival Address</th>
                                        <th data-breakpoints="xs md">Pickup Address</th>
                                        <th data-breakpoints="xs md">Order Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($pendings as $pending)
                                    <tr>
                                        <td><h5>{{$pending->name}}</h5></td>
                                        <td>{{$pending->phone}}</td> 
                                        <td><span class="col-green">&#x20A6;{{number_format($pending->cost, 2)}}</span></td>
                                        <td>{{$pending->currentadd}}</td>
                                        <td>{{$pending->pickup}}</td>
                                       
                                        <td>{{$pending->created_at->format('D-d-M-y h:i A')}}</td>
                                        
                                    </tr>
                                    <tr>
                                    </div>


                                    @endforeach
                                   
                    </div>
                                    </tr>        
                                </tbody>
                            </table>
                        </div>
                    </div>   
                    <div class="card">
                        <div class="body text-center">                            
                            <ul class="pagination pagination-primary m-b-0 text-center">
                                 {{$pendings->render()}}
                               
                            </ul>
                        </div>   
                    @else
            <p>You have no pending orders</p>
            @endif          
            </div>
        </div>
    </div>
   
</section>
