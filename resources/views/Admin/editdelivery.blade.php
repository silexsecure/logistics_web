<link rel="stylesheet" href="public/assets/css/style.min.css">
</head>

<body class="theme-blush">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="public/assets/images/loader.svg" width="48" height="48" alt="Aero"></div>
        <p>Please wait...</p>
    </div>
</div>
@extends('layouts.app')
@extends('layout.header')
<section class="content">
<div class="block-header">
        <div class="row">
       
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Update Delivery Man</h2>
                <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i>GapaNaija Logistics</a></li>
                    <li class="breadcrumb-item active">Update Profile</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">                
                <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
            </div>
        </div>
    </div>
    <div class="container bg-white">

    @if(session('success'))
                 <div class="alert alert-success">
                 <button type="button" class="close" data-dismiss="alert">×</button>
                  {{session('success')}}
                 </div>
                @endif
{!! Form::open(['action' => ['AdminController@updateD', $delivery->id], 'method' => 'POST']) !!}
<div class="file-field ">
    <div class="mb-4 text-center">
    <img   src="/storage/delivery_image/{{$delivery->image}}"   alt="img" 
        class="rounded-circle z-depth-1-half avatar-pic h-15" alt="example placeholder avatar" style="width:150px; padding-top:50px;">
    </div>
    
  </div>

<div class="form-row text-dark">
<div class="form-group col-md-6">
    <label for="inputAddress">Full Name</label>
    <input type="text" class="form-control" id="inputAddress" name="fullname" value="{{ $delivery->name }}" placeholder="Full Name">
  </div>
  
    <div class="form-group col-md-6">
      <label for="inputEmail4">Email</label>
      <input type="email" class="form-control" id="inputEmail4" name="email" value="{{ $delivery->email }}" placeholder="Email">
    </div>
    
  <div class="form-group col-md-6">
    <label for="inputAddress">Phone Number</label>
    <input type="text" class="form-control" id="inputAddress" name="phone" value="{{ $delivery->phone }}"  placeholder="Phone Number">
  </div>
  <div class="form-group col-md-6">
    <label for="inputAddress2">State</label>
    <input type="text" class="form-control" id="inputAddress2" name="state" value="{{ $delivery->state }}" placeholder="State">
  </div>
  
    <div class="form-group col-md-6">
    <label for="inputAddress2">Account Number</label>
    <input type="text" class="form-control" id="inputAddress2" name="" value="{{ $delivery->account_number }}" readonly>
  </div>
  
    <div class="form-group col-md-6">
    <label for="inputAddress2">Account Bank</label>
    <input type="text" class="form-control" id="bank_name" name="" value="{{ $delivery->account_bank }}" readonly>
  </div>
  
  
    <div class="form-group col-md-6">
    <label for="inputAddress2">Account Name</label>
    <input type="text" class="form-control" id="inputAddress2" name="" value="{{ $delivery->account_name }}" readonly>
  </div>
  
  </div>
  <div class="form-row text-dark">
  

   
    <div class="form-group col-md-6">
      <label for="inputPassword4">Password is encrypted</label>
      <input type="text" class="form-control" id="inputPassword4"  value="{{ $delivery->password }}" placeholder="Password">
    </div>

    <div class="form-group col-md-6">
      <label for="inputPassword4"> New Password</label>
      <input type="text" class="form-control" id="inputPassword4"  name="password" placeholder="Password">
    </div>

  </div>
  
    {{Form::hidden('_method', 'patch')}}
    {{Form::submit('Update',['class'=>'btn btn-primary'])}}
{!! Form::close() !!}
  <div class="row">
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body bg-white shadow-sm rounded">
        <h5 class="card-title text-black mt-5 text-center"><i class="zmdi zmdi-book-image"></i> Total Orders</h5>
        <p class="card-text text-black text-center">{{DB::table("orders")->where(["user_id"=>$delivery->id])->count()}}</p>
   
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body bg-white shadow-sm rounded">
        <h5 class="card-title text-black mt-5 text-center"><i class="zmdi zmdi-balance-wallet"></i> Wallet</h5>
        <p class="card-text text-black text-center">NGN{{$delivery->wallet}}</p>
        
      </div>
    </div>
  </div>
  
   <div class="col-sm-6">
    <div class="card">
      <div class="card-body bg-white shadow-sm rounded">
        <h5 class="card-title text-black mt-5 text-center"><i class="zmdi zmdi-money-box"></i> Total Earning</h5>
        <p class="card-text text-black text-center">NGN{{DB::table("orders")->where(["delivery_id"=>$delivery->id])->sum("total_paid")}}</p>
        
      </div>
    </div>
  </div>
</div>
                    <div class="card">
                        <h3 class="card-title text-black">Orders</h3>
                        <div class="table-responsive">
                            <table class="table table-hover product_item_list c_table theme-color mb-0">
                                <thead>
                                    <tr> 
                                        <th data-breakpoints="sm xs">Pickup</th>
                                        <th data-breakpoints="xs">Cost</th>
                                        
                                        <th data-breakpoints="xs md">Arrival Address</th>
                                        <th data-breakpoints="xs md">Pickup Address</th>
                                        <th data-breakpoints="xs md">Order Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $pending)
                                    <tr>  
                                        <td><span class="text-muted">{{$pending->depart}}</span></td>
                                        <td><span class="col-green">&#x20A6;{{number_format($pending->cost, 2)}}</span></td>
                                        <td>{{$pending->depart}}</td>
                                        <td>{{$pending->currentadd}}</td>
                                       
                                        <td>{{$pending->created_at->format('D-d-M-y h:i A')}}</td>
                                        
                                    </tr>
                                    <tr>
                                    </div>


                                    @endforeach
                                   
                    </div>
                                    </tr>        
                                </tbody>
                            </table>
                        </div>
                    </div>  
  
  
  <select
   id="bankCode"
   name="bankCode"
   class="d-none"
   >
   <option value="">--Select Bank--</option>
   <option value="801">
      Abbey Mortgage Bank
   </option>
   <option value="044">
      Access Bank
   </option>
   <option value="063">
      Access Bank (Diamond)
   </option>
   <option value="035A">
      ALAT by WEMA
   </option>
   <option value="401">
      ASO Savings and Loans
   </option>
   <option value="50931">
      Bowen Microfinance Bank
   </option>
   <option value="50823">
      CEMCS Microfinance Bank
   </option>
   <option value="023">
      Citibank Nigeria
   </option>
   <option value="559">
      Coronation Merchant Bank
   </option>
   <option value="050">
      Ecobank Nigeria
   </option>
   <option value="562">
      Ekondo Microfinance Bank
   </option>
   <option value="50126">
      Eyowo
   </option>
   <option value="070">
      Fidelity Bank
   </option>
   <option value="51314">
      Firmus MFB
   </option>
   <option value="011">
      First Bank of Nigeria
   </option>
   <option value="214">
      First City Monument Bank
   </option>
   <option value="501">
      FSDH Merchant Bank Limited
   </option>
   <option value="00103">
      Globus Bank
   </option>
   <option value="058">
      Guaranty Trust Bank
   </option>
   <option value="51251">
      Hackman Microfinance Bank
   </option>
   <option value="50383">
      Hasal Microfinance Bank
   </option>
   <option value="030">
      Heritage Bank
   </option>
   <option value="51244">
      Ibile Microfinance Bank
   </option>
   <option value="50457">
      Infinity MFB
   </option>
   <option value="301">
      Jaiz Bank
   </option>
   <option value="082">
      Keystone Bank
   </option>
   <option value="50211">
      Kuda Bank
   </option>
   <option value="90052">
      Lagos Building Investment Company Plc.
   </option>
   <option value="50563">
      Mayfair MFB
   </option>
   <option value="50304">
      Mint MFB
   </option>
   <option value="565">
      One Finance
   </option>
   <option value="999991">
      PalmPay
   </option>
   <option value="526">
      Parallex Bank
   </option>
   <option value="311">
      Parkway - ReadyCash
   </option>
   <option value="999992">
      Paycom
   </option>
   <option value="50746">
      Petra Mircofinance Bank Plc
   </option>
   <option value="076">
      Polaris Bank
   </option>
   <option value="101">
      Providus Bank
   </option>
   <option value="502">
      Rand Merchant Bank
   </option>
   <option value="125">
      Rubies MFB
   </option>
   <option value="51310">
      Sparkle Microfinance Bank
   </option>
   <option value="221">
      Stanbic IBTC Bank
   </option>
   <option value="068">
      Standard Chartered Bank
   </option>
   <option value="232">
      Sterling Bank
   </option>
   <option value="100">
      Suntrust Bank
   </option>
   <option value="302">
      TAJ Bank
   </option>
   <option value="51211">
      TCF MFB
   </option>
   <option value="102">
      Titan Bank
   </option>
   <option value="032">
      Union Bank of Nigeria
   </option>
   <option value="033">
      United Bank For Africa
   </option>
   <option value="215">
      Unity Bank
   </option>
   <option value="566">
      VFD Microfinance Bank Limited
   </option>
   <option value="035">
      Wema Bank
   </option>
   <option value="057">
      Zenith Bank
   </option>
</select>
</div>
<script>

(function() {
  setTimeout(function(){
        $("#bankCode").val("{{$delivery->account_bank}}")
        const bank = $("#bankCode").find(":selected").text()
        $("#bank_name").val(bank)
  },3000)

})();
  
</script>