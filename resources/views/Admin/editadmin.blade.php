

<body class="theme-blush">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="public/assets/images/loader.svg" width="48" height="48" alt="Aero"></div>
        <p>Please wait...</p>
    </div>
</div>
@extends('layout.header')
@extends('layouts.app')
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<section class="content">
    <div class="body_scroll">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Profile Edit</h2>
                    <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i>GapaNaija Logistics</a></li>
                        <li class="breadcrumb-item">Edit</li>
                       
                    </ul>
                    <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">                
                    <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
                    <a href="admin" class="btn btn-info btn-icon float-right"><i class="zmdi zmdi-check"></i></a>
                </div>
            </div>
        </div> 
        <div class="row clearfix">
                <div class="col-sm-12 col-md-12 col-lg-12">
                @if(session('success'))
                 <div class="alert alert-success">
                 <button type="button" class="close" data-dismiss="alert">×</button>
                  {{session('success')}}
                 </div>
                @endif
        {!! Form::open(['action' => ['AdminController@update', $admin->id], 'method' => 'POST']) !!}

        <div class="container-fluid">
        <div class="file-field ">
    <div class="mb-4 text-center">
    <img   src="public/storage/admin_image/{{$admin->image}}"   alt="img" 
        class="rounded-circle z-depth-1-half avatar-pic h-15" alt="example placeholder avatar" style="width:150px; padding-top:50px;">
    </div>
    
  </div>
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2><strong>Security</strong> Settings</h2>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-lg-4 col-md-12">
                                    <div class="form-group">
                                    <label for="inputAddress">Email</label>
                                        <input type="text" class="form-control" placeholder="Email" name="email" value="{{ $admin->email }}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12">
                                    <div class="form-group">
                                    <label for="inputAddress">Current Password</label>
                                        <input type="password" class="form-control" placeholder="Current Password"   value="{{ $admin->password }}" disabled>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12">
                                    <div class="form-group">
                                    <label for="inputAddress">New Password</label>
                                        <input type="password" class="form-control" name="password" placeholder="New Password" required>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12">
                                    <div class="form-group">
                                    <label for="inputAddress">Full Name</label>
                                        <input type="text" class="form-control" placeholder="Name" name="name" value="{{ $admin->name }}">
                                    </div>
                                </div>
                                <div class="col-12">
                                {{Form::hidden('_method', 'patch')}}
                               {{Form::submit('Save Changes',['class'=>'btn btn-info'])}}
                               {!! Form::close() !!}                                
                               </div>                                
                            </div>                              
                        </div>
                    </div>
                   
                               
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
