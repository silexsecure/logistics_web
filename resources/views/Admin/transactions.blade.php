
@extends('layout.header')
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="public/assets/images/loader.svg" width="48" height="48" alt="Aero"></div>
        <p>Please wait...</p>
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>

@extends('layouts.app')
<!-- Main Content -->


<section class="content">
<div class="container-fluid">
<div class="block-header">
            <div class="row clearfix">
       <div class="col-lg-7 col-md-6 col-sm-12">
           <h2>Transactions</h2>
           <ul class="breadcrumb">
           <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i>GapaNaija Logistics</a></li>
              
           </ul>
           <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
       </div>
       <div class="col-lg-5 col-md-6 col-sm-12">                
           <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
       </div>
   </div>
</div>
</div>
                <div class="col-lg-12"> 
                
                <div class="row">
  <div class="col-sm-6">
    <div class="card bg-white">
      <div class="card-body">
        <h5 class="card-title text-black"> <a href=""><i class="zmdi zmdi-balance-wallet" style="font-size:70px"></i></a></h5>
        <a href="/trans" class="btn btn-primary">Transactions</a>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="card bg-white">
      <div class="card-body">
        <h5 class="card-title text-black"> <a href=""><i class="zmdi zmdi-money-box" style="font-size:70px"></i></a></h5>
        <a href="/withdrawal" class="btn btn-primary">Withdrawals</a>
      </div>
    </div>
  </div>
</div>

                

                </div>
    </div>
   
</section>
