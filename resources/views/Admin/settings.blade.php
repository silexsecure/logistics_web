

<body class="theme-blush">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="public/assets/images/loader.svg" width="48" height="48" alt="Aero"></div>
        <p>Please wait...</p>
    </div>
</div>
@extends('layout.header')
@extends('layouts.app')
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<section class="content">
    <div class="body_scroll">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    
                    <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i>GapaNaija Logistics</a></li>
                        <li class="breadcrumb-item">Logistics Edit</li>
                       
                    </ul>
                  
                </div>
              
            </div>
        </div> 
        <div class="row clearfix">
                <div class="col-sm-12 col-md-12 col-lg-12">
                @if(session('success'))
                 <div class="alert alert-success">
                 <button type="button" class="close" data-dismiss="alert">×</button>
                  {{session('success')}}
                 </div>
                @endif
       
         {!! Form::open(['action' => ['AdminController@saveLogisticPrice'], 'method' => 'post']) !!}
        <div class="container-fluid">
        <div class="file-field ">
   
    
  </div>
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                       
                        <div class="body">
                            <div class="row">
                                <div class="col-lg-4 col-md-12">
                                    <div class="form-group">
                                    <label for="inputAddress">Logistics Amount Per KM</label>
                                        <input type="text" class="form-control" placeholder="Enter Amount" name="amountperkm" value="{{$amountperkm->price}}">
                                    </div>
                                </div>
                              
                                <div class="col-12">
                                {{Form::hidden('_method', 'post')}}
                               {{Form::submit('Save Changes',['class'=>'btn btn-info'])}}
                               {!! Form::close() !!}                                
                               </div>                                
                            </div>                              
                        </div>
                    </div>
                   
                               
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
