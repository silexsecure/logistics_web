<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>:: Admin :: Sign In</title>
<script src="{{ asset('assets/bundles/libscripts.bundle.js') }}" defer></script>
<script src="{{ asset('assets/bundles/vendorscripts.bundle.js') }}" defer></script>
<!-- Favicon-->
<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- Custom Css -->
<link href="{{ asset('public/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('public/assets/css/style.min.css') }}" rel="stylesheet">

</head>

<div class="authentication">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-12">
                <div class="card auth_form">
                <div class="header">
                    <img src="{{URL::asset('public/img/Eat-naija.png')}}" alt="Sign In"/>
                        <h5>EatNaija Logistics</h5>
                   
                   <a href="login/admin" class="btn btn-md btn-primary">Admin</a>
                   <a href="login/delivery"  class="btn btn-md btn-info">Delivery Man</a>
                    </div>
                <div class="copyright text-center">
                    &copy;
                    <script>document.write(new Date().getFullYear())</script>,
                    <span>Developed by SilexSecure</a></span>
                </div>
            </div>
            </div>
            <div class="col-lg-8 col-sm-12">
                <div class="card">
                    <img src="{{URL::asset('public/assets/images/signin.svg')}}" alt="Sign In"/>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="{{ asset('public/assets/bundles/libscripts.bundle.js') }}" defer></script>
<script src="{{ asset('public/assets/bundles/vendorscripts.bundle.js') }}" defer></script>

</body>
</html>

