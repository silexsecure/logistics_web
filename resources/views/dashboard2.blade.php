<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="public/assets/images/loader.svg" width="48" height="48" alt="Aero"></div>
        <p>Please wait...</p>
    </div>
</div>

@extends('layouts.app')
@extends('layout.headerD')

<!-- Main Content -->

<section class="content">
<div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Dashboard</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i>GapaNaija Logistics</a></li>
                    <li class="breadcrumb-item active">Dashboard 1</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">                
          @if(Auth::guard("deliver")->user()->account_number == null)
                <button class="btn btn-primary btn-icon float-right w-100" type="button" data-toggle="modal" data-target="#updateaccount"><i class="zmdi zmdi-plus"></i> Update Account Number</button>

          @endif
                      </div>
        </div>
    </div>
    @if(session('success'))
                 <div class="alert alert-success">
                 <button type="button" class="close" data-dismiss="alert">×</button>
                  {{session('success')}}
                 </div>
                @endif
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 ">
                    <div class="body">
                        <h5>Pending Orders</h5>
                        <h2>{{ $pending->count() }}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script></small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 ">
                    <div class="body">
                        <h5>Ongoing Orders</h5>
                        <h2>{{ $ongoing->count() }}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script> </small>

                       </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 ">
                    <div class="body">
                        <h5>Delivered Orders</h5>
                        <h2>{{ $delivered->count() }}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script> </small>

                       </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 ">
                    <div class="body">
                        <h5>Canceled Orders </h5>
                        <h2>{{ $rejected->count() }}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script> </small>

                    </div>
                </div>
            </div>


            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 ">
                    <div class="body">
                        <h5>Today's Trips </h5>
                        <h2>{{ $orders_today->count() }}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script> </small>

                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 ">
                    <div class="body">
                        <h5> Trips This Month</h5>
                        <h2>{{ $orders_present->count() }}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script> </small>

                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 ">
                    <div class="body">
                        <h5> Trips last Month </h5>
                        <h2>{{ $orders_last->count() }}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script> </small>

                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 ">
                    <div class="body">
                        <h5>Amount received Today </h5>
                        <h2>&#8358 {{number_format($today, 2)}}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script> </small>

                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 ">
                    <div class="body">
                        <h5>Amount received this month </h5>
                        <h2>&#8358 {{number_format($present, 2)}}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script> </small>

                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2">
                    <div class="body">
                        <h5>Amount received last month </h5>
                        <h2>&#8358 {{number_format($last, 2)}}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script> </small>

                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2">
                    <div class="body">
                        <h5>Total Amount received </h5>
                        <h2>&#8358 {{number_format($total, 2)}}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script> </small>

                    </div>
                </div>
            </div>
       

        <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 ">
                    <div class="body">
                        <h5> Subscription Plan </h5>
                        <h5>&#8358 {{number_format($balance, 2)}} <small>Available</small></h5>
                        <form>
  <script src="https://checkout.flutterwave.com/v3.js"></script>
  <button class="btn btn-md btn-info" type="button" onClick="makePayment()">Payment of &#8358 1000</button>
</form>
                      
                    </div>
                </div>
            </div>
                </div>
            </div>
        </div>
        </div>


   
  </div>
                </div>
            </div>
        </div>
        
            </div>
        </div>
  
</section>

<div class="modal fade" id="updateaccount" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Account Number</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="update_account">
      <div class="modal-body">
        <div class="form-group">
            <label>Account Number</label>
            <input type="text" class="form-control" id="account_number">
            
        </div>
         
        
        <div class="">
            <label>Select Account Bank</label>
                <select class="form-control" id="account_bank" >
                                                        <option selected disabled>Choose Bank</option>
                                                        <option value="044">Access Bank</option>
                                                        <option value="023">  Citibank</option>
                                                        <option value="050">Ecobank</option>
                                                        <option value="214">First City Monument Bank (FCMB)</option>
                                                        <option value="070">Fidelity Bank</option>
                                                        <option value="011">First Bank</option>
                                                       
                                                        <option value="058">Guaranty Trust Bank (GTB)</option>
                                                        <option value="030">Heritage Bank</option>
                                                        <option value="301">Jaiz Bank</option>
                                                        <option value="082">Keystone Bank</option>
                                                        <option value="526">Parallex Bank</option>
                                                        <option value="101">Providus Bank</option>
                                                        <option value="221">Stanbic IBTC Bank</option>
                                                        <option value="076">Skye Bank</option>
                                                        <option value="068">Standard Chartered Bank</option>
                                                        <option value="232">Sterling Bank</option>
                                                        <option value="100">Suntrust Bank</option>
                                                        <option value="102"> Titan Trust Bank</option>
                                                        <option value="032">Union Bank</option>
                                                        <option value="033">United Bank for Africa (UBA)</option>
                                                        <option value="215">Unity Bank</option>
                                                        <option value="035">Wema Bank</option>
                                                        <option value="052">Zenith Bank</option>
                                                    </select>
        </div>
        
           <div class="form-group" style="display:none" id="acname">
            <label>Account Name</label>
            <input type="text" class="form-control" id="account_name" readonly>
            
        </div>
        <a href="#" onclick="validate()">validate</a>
      </div>
      <div class="modal-footer " id="cc" style="display:none" > 
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
@php
                  $public_key = config("app.flw_public_key");
                  $email = Auth::guard('deliver')->user()->email;
                  $name = Auth::guard('deliver')->user()->name;
                  $phone_number = Auth::guard('deliver')->user()->phone;
                  $random_string = Str::random(10);
                  $user_id = Auth::guard('deliver')->user()->id;
                  
                @endphp

                <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                <script>
                    const API_publicKey = '{!! $public_key !!}';
                    var email = '{!! $email !!}';                  
                    var phone_number = '{!! $phone_number !!}';
                    var random_string = '{!! $random_string !!}';
                    var user_id = '{!! $user_id !!}';
                    var amount = 1000;
                   
                    
                  function setUpPaymentForm(){
                    $(document).ready(function(){
                      $('#myModal').modal('show'); 
                    });
                  }
                  function makePayment() {
    FlutterwaveCheckout({
      public_key: "FLWPUBK_TEST-8e53a643a6ee9a02a9cdd966eef6e8f8-X",
      tx_ref: "log-"+random_string,
      amount: 1000,
      currency: "NGN",
      country: "NG",
      payment_options: "card,mobilemoney,ussd",
      customer: {
        email: email,
         name: name,
         phone: phone_number,
      },
                      onclose: function() {},
                      callback: function(response) {
                      var txref = response.txRef; // collect txRef returned and pass to a 					server page to complete status check.
                      console.log("This is the response returned after a charge", response);
                     
                        $.ajaxSetup({
                          headers: {
                              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                              }
                          });
                          $.ajax({
                            url: '{{ url("subscription") }}',
                            type: 'POST', 
                            cache: false,
                            beforeSend: function(){
                              $('.ajax-loader').show();
                            },
                            data:{ 
                            _token: $('#token').val(),
                            userid: user_id,
                            amount:amount,
                          
                            txref: txref,
                           
                            },
                            complete: function(){
                              $('.ajax-loader').hide();
                            },
                            success: function(data){
                              console.log(data);
                              window.location = "{{ url('/dashboard2') }}";
                             
                            },
                            error: function(ts) { alert(ts.responseText)
                                console.log(data);
                              window.location = "{{ url('/dashboard2') }}"; }
  
	})
	



                        
                     
                   
                  }
              });
                  }
                  
                  
                  function validate(){
                      const account_number = $("#account_number").val()
                      const account_bank = $("#account_bank").find(":selected").val()
                      $.post('/validateacct',{
                          account_number,
                          account_bank,
                          "_token":"{{csrf_token()}}"
                      }).then(function(res){
                          $("#acname").show()
                           $("#cc").show()
                           $("#account_name").val(res.name)
                          
                      }).fail(function(res){
                          swal(res.responseJSON.message,res.responseJSON.error,"error")
                      })
                  }
                  
        
        
        (function() {
      setTimeout(()=>{
                          $("form#update_account").submit(function(){
                      event.preventDefault()
                      $("button").attr("disabled",true)
                      const account_number = $("#account_number").val()
                      const account_bank = $("#account_bank").find(":selected").val()
                        const account_name = $("#account_name").val() 
                        
                        $.post('/update_account',{
                          account_number,
                          account_bank,
                          account_name,
                          "_token":"{{csrf_token()}}"
                      }).then(function(res){
                             swal("",res.message,"success")
                          location.reload()
                      }).fail(function(res){
                           $("button").attr("disabled",false)
                          swal("",res.responseJSON.message,"error")
                      })
                  })
            },3000)

})();
                   
                </script>

<style>
    .bootstrap-select>.dropdown-toggle {
     display:none;   
    }
</style>



