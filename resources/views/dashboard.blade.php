<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="public/assets/images/loader.svg" width="48" height="48" alt="Aero"></div>
        <p>Please wait...</p>
    </div>
</div>

@extends('layouts.app')
@extends('layout.header')

<!-- Main Content -->

<section class="content">
<div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Dashboard</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i>GapaNaija Logistics</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">                
                <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
            </div>
        </div>
    </div>
    @if(session('success'))
                 <div class="alert alert-success">
                 <button type="button" class="close" data-dismiss="alert">×</button>
                  {{session('success')}}
                 </div>
                @endif
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 big_icon sales">
                    <div class="body">
                        <h5>Pending Orders</h5>
                        <h2>{{ $pending->count() }}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script></small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 big_icon sales">
                    <div class="body">
                        <h5>Ongoing Orders</h5>
                        <h2>{{ $ongoing->count() }}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script> </small>

                       </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 big_icon sales">
                    <div class="body">
                        <h5>Delivered Orders</h5>
                        <h2>{{ $delivered->count() }}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script> </small>

                       </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 big_icon sales">
                    <div class="body">
                        <h5> Delivery Men </h5>
                        <h2>{{ $men->count() }}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script> </small>

                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 ">
                    <div class="body">
                        <h5>Today's Trips </h5>
                        <h2>{{ $orders_today->count() }}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script> </small>

                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 ">
                    <div class="body">
                        <h5> Trips This Month</h5>
                        <h2>{{ $orders_present->count() }}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script> </small>

                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 ">
                    <div class="body">
                        <h5> Trips last Month </h5>
                        <h2>{{ $orders_last->count() }}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script> </small>

                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 ">
                    <div class="body">
                        <h5>Amount received Today </h5>
                        <h2>&#8358 {{number_format($today, 2)}}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script> </small>

                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 ">
                    <div class="body">
                        <h5>Amount received this month </h5>
                        <h2>&#8358 {{number_format($present, 2)}}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script> </small>

                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2">
                    <div class="body">
                        <h5>Amount received last month </h5>
                        <h2>&#8358 {{number_format($last, 2)}}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script> </small>

                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2">
                    <div class="body">
                        <h5>Total Amount received </h5>
                        <h2>&#8358 {{number_format($total, 2)}}</h2>
                        <small> as at <script>document.write(new Date().toString("t")) </script> </small>

                    </div>
                </div>
            </div>
        </div>
                </div>
            </div>
        </div>
      


   
  </div>
                </div>
            </div>
        </div>
        
            </div>
        </div>
  
</section>
