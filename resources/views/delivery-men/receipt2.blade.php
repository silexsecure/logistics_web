<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<style>
body {
	font-family: 'Varela Round', sans-serif;
}
.modal-confirm {		
	color: #636363;
	
}
.modal-confirm .modal-content {
	padding: 20px;
	border-radius: 5px;
	border: none;
	text-align: center;
	font-size: 14px;
}
.modal-confirm .modal-header {
	border-bottom: none;   
	position: relative;
}
.modal-confirm h4 {
	text-align: center;
	font-size: 26px;
	margin: 30px 0 -10px;
}
.modal-confirm .close {
	position: absolute;
	top: -5px;
	right: -2px;
}
.modal-confirm .modal-body {
	color: #999;
    width:450px;
}
.modal-confirm .modal-footer {
	border: none;
	text-align: center;		
	border-radius: 5px;
	font-size: 13px;
	padding: 10px 15px 25px;
}
.modal-confirm .modal-footer a {
	color: #999;
}		
.modal-confirm .icon-box {
	width: 80px;
	height: 80px;
	margin: 0 auto;
	border-radius: 50%;
	z-index: 9;
	text-align: center;
	border: 3px solid #f15e5e;
}
.modal-confirm .icon-box i {
	color: #f15e5e;
	font-size: 46px;
	display: inline-block;
	margin-top: 13px;
}
.modal-confirm .btn, .modal-confirm .btn:active {
	color: #fff;
	border-radius: 4px;
	background: #60c7c1;
	text-decoration: none;
	transition: all 0.4s;
	line-height: normal;
	min-width: 120px;
	border: none;
	min-height: 40px;
	border-radius: 3px;
	margin: 0 5px;
}
.modal-confirm .btn-secondary {
	background: #c1c1c1;
}
.modal-confirm .btn-secondary:hover, .modal-confirm .btn-secondary:focus {
	background: #a8a8a8;
}
.modal-confirm .btn-danger {
	background: #f15e5e;
}
.modal-confirm .btn-danger:hover, .modal-confirm .btn-danger:focus {
	background: #ee3535;
}
.trigger-btn {
	display: inline-block;
	margin: 100px auto;
}
</style>
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="public/assets/images/loader.svg" width="48" height="48" alt="Aero"></div>
        <p>Please wait...</p>
    </div>
</div>
@extends('layout.headerD')
<!-- Overlay For Sidebars -->
<div class="overlay"></div>

@extends('layouts.app')
<!-- Main Content -->


<section class="content">
<div class="container-fluid">
<div class="block-header">
<div class="row clearfix">
       <div class="col-lg-7 col-md-6 col-sm-12">
           <h2>Order Receipt</h2>
           <ul class="breadcrumb">
           <li class="breadcrumb-item"><a href="dashboard2"><i class="zmdi zmdi-home"></i>GapaNaija Logistics</a></li>
              
           </ul>
           <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
       </div>
       <div class="col-lg-5 col-md-6 col-sm-12">                
           <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
       </div>
   </div>
</div>
</div>

<div class="card">
<div class="text-right">
<a class="btn btn-info text-white waves-effect waves-float btn-sm waves-green" class="btn btn-primary">Ongoing Order</a>
</div>
                        <div class="body text-striped text-left">   
                        <table class="table table-striped" >
  <tr>
    <th>Customer Name:</th>
    <td><span class="text-right ">{{$pending->name}}<span></td>
  </tr>
  <tr>
    <th>Phone number:</th>
    <td>{{$pending->phone}}</td>
  </tr>
  <tr>
    <th>Pickup:</th>
    <td>{{$pending->pickup}}</td>
  </tr>
  <tr>
    <th>Price:</th>
    <td>&#x20A6;{{number_format($pending->cost, 2)}}</td>
  </tr>
  <tr>
    <th>Pickup Address:</th>
    <td>{{$pending->depart}}</td>
  </tr>
  <tr>
    <th>Arrival Address:</th>
    <td>{{$pending->currentadd}}</td>
  </tr>
  <tr>
    <th>Payment Status:</th>
    <td>{{$status}}</td>
  </tr>
  <tr>
    <th>Order Time:</th>
    <td>{{$pending->created_at->format('D-d-M-y h:i A')}}</td>
  </tr>
</table>                         
                            
<!--{!! Form::open(['action' =>['DeliveryController@approve', $pending->id], 'method'=> 'POST'])!!}
{{Form::button('Accept',['class'=>'btn btn-primary btn btn-default waves-effect waves-float btn-sm waves-red', 'type'=>'submit'])}}
{!!Form::close()!!}     
                               
                                  <p>     {!! Form::open(['action' =>['DeliveryController@done', $pending->id], 'method'=> 'POST'])!!}
{{Form::button('Delivered',['class'=>'btn btn-primary ', 'type'=>'submit'])}}
{!!Form::close()!!}-->

<button type="submit" href="#myModal{{$pending->id}}"  data-toggle="modal" class="btn btn-outline-danger btn-sm" >
                                                  Delivered
                                                    </button>


<div id="myModal{{$pending->id}}" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header flex-column">
				<div class="icon-box">
					<i class="material-icons">&#xeb3c;</i>
				</div>						
				<h4 class="modal-title">Confirm Delivery</h4>	
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body w100">
				<p>Enter Customer delivery code to confirm delivery</p>
                <form >
                <input class="form-control" id="codetxt" type="text" placeholder="Enter Code" name="code">
                <br>
                <button id="view{{$pending->id}}"  type="button" class="btn btn-danger">Submit</button>
			</form>
			</div>
			<div class="modal-footer justify-content-center">
				
			
	</div>
		</div>
	</div>
</div>   

									<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
     $("#view{{$pending->id}}").click(function(event) {
	// prevent the usual form submission behaviour; the "action" attribute of the form
    var code = document.getElementById("codetxt").value;

	event.preventDefault();
	$.ajax({
		'url':'delivered_order/{{$pending->id}}',
		// all of your POST/GET variables
		  data:{
		     code:code,
		       "_token":"{{csrf_token()}}"
		      
		  },
		'type': 'post',
		 success: function(data){
  
         console.log(data);
		 },
		  error: function(ts) { alert(ts.responseText) }
	})
	.done( function (response) {
		swal(response.message, "", response.status);
        if ( response.status === 'success'){
			window.location = "{{ url('delivered2') }}";
        }
       
	})
	.fail( function (code, status) {
			swal("Unable to view order!", "", "error");
	})

 
})
 

</script>

                        </div>
                        
                  
                </div>
                    </div>    
                   

                    </div>
                    </div>
                    </section>