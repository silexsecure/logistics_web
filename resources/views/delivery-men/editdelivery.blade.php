<link rel="stylesheet" href="public/assets/css/style.min.css">
</head>

<body class="theme-blush">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="public/assets/images/loader.svg" width="48" height="48" alt="Aero"></div>
        <p>Please wait...</p>
    </div>
</div>
@extends('layouts.app')
@extends('layout.headerD')
<section class="content">
<div class="block-header">
        <div class="row">
       
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Update Delivery Man</h2>
                <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="dashboard"><i class="zmdi zmdi-home"></i>GapaNaija Logistics</a></li>
                    <li class="breadcrumb-item active">Update Profile</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">                
                <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
            </div>
        </div>
    </div>
    <div class="container bg-white">

    @if(session('success'))
                 <div class="alert alert-success">
                 <button type="button" class="close" data-dismiss="alert">×</button>
                  {{session('success')}}
                 </div>
                @endif
{!! Form::open(['action' => ['DeliveryController@updateD', $delivery->id], 'method' => 'POST']) !!}
<div class="file-field ">
    <div class="mb-4 text-center">
    <img src="public/storage/delivery_image/{{Auth::guard('deliver')->user()->image}}"   alt="img" 
        class="rounded-circle z-depth-1-half avatar-pic h-15" alt="example placeholder avatar" style="width:150px; padding-top:50px;">
    </div>
    
  </div>

<div class="form-row text-dark">
<div class="form-group col-md-6">
    <label for="inputAddress">Full Name</label>
    <input type="text" class="form-control" id="inputAddress" name="fullname" value="{{ $delivery->name }}" placeholder="Full Name">
  </div>
  
    <div class="form-group col-md-6">
      <label for="inputEmail4">Email</label>
      <input type="email" class="form-control" id="inputEmail4" name="email" value="{{ $delivery->email }}" placeholder="Email">
    </div>
    
  <div class="form-group col-md-6">
    <label for="inputAddress">Phone Number</label>
    <input type="text" class="form-control" id="inputAddress" name="phone" value="{{ $delivery->phone }}"  placeholder="Phone Number">
  </div>
  <div class="form-group col-md-6">
    <label for="inputAddress2">State</label>
    <input type="text" class="form-control" id="inputAddress2" name="state" value="{{ $delivery->state }}" placeholder="State">
  </div>
  </div>
  <div class="form-row text-dark">
  

   
    <div class="form-group col-md-6">
      <label for="inputPassword4">Password is encrypted</label>
      <input type="text" class="form-control" id="inputPassword4"  value="{{ $delivery->password }}" placeholder="Password">
    </div>

    <div class="form-group col-md-6">
      <label for="inputPassword4"> New Password</label>
      <input type="text" class="form-control" id="inputPassword4"  name="password" placeholder="Password">
    </div>

  </div>
    {{Form::hidden('_method', 'patch')}}
    {{Form::submit('Update',['class'=>'btn btn-primary'])}}
{!! Form::close() !!}
</div>