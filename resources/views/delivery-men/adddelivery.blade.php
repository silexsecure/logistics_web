<link rel="stylesheet" href="public/assets/css/style.min.css">
</head>

<body class="theme-blush">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="public/assets/images/loader.svg" width="48" height="48" alt="Aero"></div>
        <p>Please wait...</p>
    </div>
</div>
@extends('layouts.app')
@extends('layout.header')
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<section class="content">
<div class="container-fluid">
<div class="block-header">
        <div class="row">
       
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Add Delivery Man</h2>
                <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="dashboard2"><i class="zmdi zmdi-home"></i>GapaNaija Logistics</a></li>
                    <li class="breadcrumb-item active">Add Delivery Man</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">                
                <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
            </div>
        </div>
    </div>
    <div class="container bg-white">

@if(session('success'))
             <div class="alert alert-success">
              {{session('success')}}
             </div>
            @endif
{!! Form::open(['action' => 'Auth\RegisterController@createdelivery', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}


<div class="file-field ">
<div class="mb-4 text-center">
  <img src="assets/images/60111.jpg"
    class="rounded-circle z-depth-1-half avatar-pic h-15" alt="example placeholder avatar" style="width:100px; padding-top:50px;">
</div>
<div class="d-flex justify-content-center">
  <div class="btn btn-mdb-color btn-rounded float-left">
    <span><input type="file" name="image"></span>
  </div>
</div>
</div>

<div class="form-row">
<div class="form-group col-md-6">
<label for="inputAddress">Full Name</label>
<input type="text" class="form-control" id="inputAddress" name="name" placeholder="Full Name" required>
</div>

<div class="form-group col-md-6">
  <label for="inputEmail4">Email</label>
  <input type="email" class="form-control" id="inputEmail4" name="email" placeholder="Email" required>
</div>

<div class="form-group col-md-6">
<label for="inputAddress">Phone Number</label>
<input type="text" class="form-control" id="inputAddress" name="phone" placeholder="Phone Number" required>
</div>
<div class="form-group col-md-6">
<label for="inputAddress2">State</label>
<input type="text" class="form-control" id="inputAddress2" name="state" placeholder="State" required>
</div>
</div>
<div class="form-row">



<div class="form-group col-md-6">
  <label for="inputPassword4">Password</label>
  <input type="text" class="form-control" id="inputPassword4" name="password" placeholder="Password" required>
</div>
<div class="form-group col-md-4">
  <label for="inputPassword4">Confirm Password</label>
  <input type="text" class="form-control"  name="confirmpassword" placeholder="Confirm Password" required>
</div>
</div>
<button type="submit" class="btn btn-primary">Submit</button>
{!! Form::close() !!}
</div>
</section>
