
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<style>
body {
	font-family: 'Varela Round', sans-serif;
}
.modal-confirm {		
	color: #636363;
	
}
.modal-confirm .modal-content {
	padding: 20px;
	border-radius: 5px;
	border: none;
	text-align: center;
	font-size: 14px;
}
.modal-confirm .modal-header {
	border-bottom: none;   
	position: relative;
}
.modal-confirm h4 {
	text-align: center;
	font-size: 26px;
	margin: 30px 0 -10px;
}
.modal-confirm .close {
	position: absolute;
	top: -5px;
	right: -2px;
}
.modal-confirm .modal-body {
	color: #999;
    width:450px;
}
.modal-confirm .modal-footer {
	border: none;
	text-align: center;		
	border-radius: 5px;
	font-size: 13px;
	padding: 10px 15px 25px;
}
.modal-confirm .modal-footer a {
	color: #999;
}		
.modal-confirm .icon-box {
	width: 80px;
	height: 80px;
	margin: 0 auto;
	border-radius: 50%;
	z-index: 9;
	text-align: center;
	border: 3px solid #f15e5e;
}
.modal-confirm .icon-box i {
	color: #f15e5e;
	font-size: 46px;
	display: inline-block;
	margin-top: 13px;
}
.modal-confirm .btn, .modal-confirm .btn:active {
	color: #fff;
	border-radius: 4px;
	background: #60c7c1;
	text-decoration: none;
	transition: all 0.4s;
	line-height: normal;
	min-width: 120px;
	border: none;
	min-height: 40px;
	border-radius: 3px;
	margin: 0 5px;
}
.modal-confirm .btn-secondary {
	background: #c1c1c1;
}
.modal-confirm .btn-secondary:hover, .modal-confirm .btn-secondary:focus {
	background: #a8a8a8;
}
.modal-confirm .btn-danger {
	background: #f15e5e;
}
.modal-confirm .btn-danger:hover, .modal-confirm .btn-danger:focus {
	background: #ee3535;
}
.trigger-btn {
	display: inline-block;
	margin: 100px auto;
}
</style>


@extends('layout.headerD')
<!-- Overlay For Sidebars -->
<div class="overlay"></div>

@extends('layouts.app')
<!-- Main Content -->
<body >

<section class="content" >
<div class="container-fluid">
<div class="block-header">
            <div class="row clearfix">
       <div class="col-lg-7 col-md-6 col-sm-12">
           <h2>Pending Orders</h2>
           <ul class="breadcrumb">
               <li class="breadcrumb-item"><a href="dashboard2"><i class="zmdi zmdi-home"></i>GapaNaija Logistics</a></li>
              
           </ul>
           <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
       </div>
       <div class="col-lg-5 col-md-6 col-sm-12">                
           <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
       </div>
   </div>
</div>
</div>
                <div class="col-lg-12">
                @if(session('success'))
                 <div class="alert alert-success">
                  {{session('success')}}
                 </div>
                @endif
                @if(count($data)>0)
                <button class="btn" onclick="page()">print</button>

                <script>
                function page(){
                 window.print()
                   }
                   </script>
				   <button type="submit" id="refresh" class="btn btn-outline-secondary btn-sm" >
                                                   Refresh
                                                    </button>
                    <div class="card">
                        <div class="table-responsive">
                            <table  class="table table-hover product_item_list c_table theme-color mb-0">
                                <thead>
                                    <tr>
                                        <th>Customer Name</th>
                                        
                                        
                                        <th data-breakpoints="xs">Pickup Address</th>
                                        <th data-breakpoints="xs">Delivery Address</th>
                                       
                                        <th data-breakpoints="xs md">Order Time</th>
                                        <th data-breakpoints="xs md">Action</th>

                                    </tr>
                                </thead>
                                <tbody id="example1">
                                @foreach($data as $pending)
                                    <tr >
                                        <td><h5>{{$pending->name}}</h5></td>
                                         
                                        <td><span class="col-green">{{$pending->pickup}}</span></td>
                                        <td><span class="col-green">{{$pending->currentadd}}</span></td>
                                       
                                        <td>{{$pending->created_at->format('F j, Y h:i A')}}</td>
                                        <td>

                               
<button type="submit" href="#myModal{{$pending->id}}"  data-toggle="modal" class="btn btn-outline-secondary btn-sm" >
                                                     View Order
                                                    </button>
                                                    </td>
 

                                      
                                    </tr>
                                   
                                    </div>

									<div id="myModal{{$pending->id}}" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header flex-column">
				<div class="icon-box">
					<i class="material-icons">&#xeb3c;</i>
				</div>						
				<h4 class="modal-title">Are you sure you want to Accept this order?</h4>	
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body w100">
				<p>If you view this order,₦50 will be deducted from your wallet, After viewing and you cant continue with the order, press cancel, ₦50 to be deducted from your wallet.</p>
			</div>
			<div class="modal-footer justify-content-center">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
				
				<button id="view{{$pending->id}}" type="button" class="btn btn-danger">Yes</button>
			
	</div>
		</div>
	</div>
</div>   

									<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
     $("#view{{$pending->id}}").click(function(event) {
	// prevent the usual form submission behaviour; the "action" attribute of the form
	event.preventDefault();
	$.ajax({
		'url':'accept_order/{{$pending->id}}',
		// all of your POST/GET variables
		  data:{
		     
		       "_token":"{{csrf_token()}}"
		      
		  },
		'type': 'post',
		 success: function(data){
  
         console.log(data);
        
		 },
		  error: function(ts) { alert(ts.responseText) }
	})
	.done( function (response) {
		swal(response.message, "", response.status);
		if ( response.status === 'success'){
			window.location = "{{ url('/view_order'.$pending->id) }}";
        }
	})
	

	.fail( function (code, status) {
			swal("Unable to view order!", "", "error");
	})

 
})
 

</script>



                                    @endforeach
                                   
                    </div>
                                        
                                </tbody>
                            </table>
                        </div>
                    </div>   
                    <div class="card">
                        <div class="body text-center">                            
                            <ul class="pagination pagination-primary m-b-0 text-center">
                                 {{$data->render()}}
                               
                            </ul>
                        </div>   
                    @else
            <p>You have no pending orders</p>
            @endif          
            </div>
        </div>
    </div>
	

<script type="text/javascript">

$("#refresh").click(function(event) {
	// prevent the usual form submission behaviour; the "action" attribute of the form
	event.preventDefault();
$.ajax({
url: "{{ url('/pendingajax') }}",
method: "GET",
dataType: 'json',
success: function(response) {
	$('#example1');
	response.data.map((d)=>{
		$('#example1').append(` <tr>
                                        <td><h5>${d.name}</h5></td>
                                        
                                        <td><span class="text-muted">${d.pickup}</span></td>
                                        <td><span class="col-green">${d.depart}</span></td>
                                        <td><span class="col-green">${d.currentadd}</span></td>
                                       
                                        <td>${d.created_at}</td>
                                        <td>

                               
<button type="submit" href="#myModal${d.id}"  data-toggle="modal" class="btn btn-outline-secondary btn-sm" >
                                                     View Order
                                                    </button>
                                                    </td>
 

                                      
                                    </tr>`);
									
})
},
error: function(ts) { alert(ts.responseText) }
	

});

});
</script>

</section>
</body>