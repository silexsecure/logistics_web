<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>GapaNaija Logistics</title>

</head>
<style>
.theme-blush .table.theme-color thead td, .theme-blush .table.theme-color thead th {
    background: #fd7e14;
}
.theme-blush .sidebar .menu .list li.active>:first-child i, .theme-blush .sidebar .menu .list li.active>:first-child span {
    color: #fd7e14;
}
.theme-blush .sidebar .menu .list li.active>:first-child i, .theme-blush .sidebar .menu .list li.active>:first-child span {
    color: #fd7e14 !important;
}
</style>
<body class="theme-blush">


<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<!-- Main Search -->


<!-- Right Icon menu Sidebar -->

</div>

<aside id="rightsidebar" class="right-sidebar">
    <ul class="nav nav-tabs sm">
    
        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#setting"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a></li>
        
        
                       
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#chat"><i class="zmdi zmdi-comments"></i></a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="setting">
            <div class="slim_scroll">
                <div class="card">
                    <h6>Theme Option</h6>
                    <div class="light_dark">
                        <div class="radio">
                            <input type="radio" name="radio1" id="lighttheme" value="light" checked="">
                            <label for="lighttheme">Light Mode</label>
                        </div>
                        <div class="radio mb-0">
                            <input type="radio" name="radio1" id="darktheme" value="dark">
                            <label for="darktheme">Dark Mode</label>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <h6>Color Skins</h6>
                    <ul class="choose-skin list-unstyled">
                        <li data-theme="purple"><div class="purple"></div></li>                   
                        <li data-theme="blue"><div class="blue"></div></li>
                        <li data-theme="cyan"><div class="cyan"></div></li>
                        <li data-theme="green"><div class="green"></div></li>
                        <li data-theme="orange"><div class="orange"></div></li>
                        <li data-theme="blush" class="active"><div class="blush"></div></li>
                    </ul>                    
                </div>
               
            </div>                
        </div>       
        </div>
        </aside>
<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">

    <div class="navbar-brand">
        <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>
        <a href="dashboard"><img src="/public/img/Gapa_new_1.png" width="25" alt="Aero"><span class="m-l-10">Deliivery Man</span></a>
    </div>
    <div class="navbar-right">
    <ul class="navbar-nav">
<li><a href="javascript:void(0);" class="js-right-sidebar" title="Setting"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a></li>
<li href="javascript:void(0);" class="nav-item  js-right-sidebar">
                               <br> 
                                    <a class="nav-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                   <span style="font-size:20px;" class="zmdi zmdi-power"></span>
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                              
                              

                               
                            </li>
</ul>
</div>
    <div class="menu">
        <ul class="list">
            <li>
          
                <div class="user-info">
                @if(Auth::guard('deliver')->check())
                    <a class="image" href="#"><img class="w50"  src="public/storage/delivery_image/{{Auth::guard('deliver')->user()->image}}"   alt="img" >
                    <div class="detail">
                        
                       
                   <h6> {{ Auth::guard('deliver')->user()->name }}</h6></a>
                   
                    @endif
                                              
                    </div>
                </div>
            </li>
           
            <li class="active "><a href="/dashboard2"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
                    <li  class=""><a href="pending2"><i class="zmdi zmdi-shopping-cart"></i><span>Pending Order</span></a></li>
                    <li  class=""><a href="ongoing2"><i class="zmdi zmdi-shopping-cart"></i><span>Ongoing Orders</span></a></li>
                    <li  class=""><a href="delivered2"><i class="zmdi zmdi-shopping-cart"></i><span>Delivered Orders</span></a></li>
                    <li  class=""><a href="rejected2"><i class="zmdi zmdi-shopping-cart"></i><span>Canceled Orders</span></a></li>
                     <li  class=""><a href="withdrawals"><i class="zmdi zmdi-group"></i><span>Withdrawals</span></a></li>
 
           
                        <!-- Authentication Links -->
                   
                    
           </ul>
           </div>

</aside>


</body>
</html>