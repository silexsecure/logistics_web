<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>GapaNaija Logistics</title>

</head>
<style>
.theme-blush .table.theme-color thead td, .theme-blush .table.theme-color thead th {
    background: #fd7e14;
}
.theme-blush .sidebar .menu .list li.active>:first-child i, .theme-blush .sidebar .menu .list li.active>:first-child span {
    color: #fd7e14;
}
.theme-blush .sidebar .menu .list li.active>:first-child i, .theme-blush .sidebar .menu .list li.active>:first-child span {
    color: #fd7e14 !important;
}
.theme-blush .sidebar .menu : hover {
    color: #fd7e14 !important;
}
</style>
<body class="theme-blush">


<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<!-- Main Search -->


<!-- Right Icon menu Sidebar -->

</div>

<aside id="rightsidebar" class="right-sidebar">
    <ul class="nav nav-tabs sm">
    
        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#setting"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a></li>
        
        
                       
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#chat"><i class="zmdi zmdi-comments"></i></a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="setting">
            <div class="slim_scroll">
                <div class="card">
                    <h6>Theme Option</h6>
                    <div class="light_dark">
                        <div class="radio">
                            <input type="radio" name="radio1" id="lighttheme" value="light" checked="">
                            <label for="lighttheme">Light Mode</label>
                        </div>
                        <div class="radio mb-0">
                            <input type="radio" name="radio1" id="darktheme" value="dark">
                            <label for="darktheme">Dark Mode</label>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <h6>Color Skins</h6>
                    <ul class="choose-skin list-unstyled">
                        <li data-theme="purple"><div class="purple"></div></li>                   
                        <li data-theme="blue"><div class="blue"></div></li>
                        <li data-theme="cyan"><div class="cyan"></div></li>
                        <li data-theme="green"><div class="green"></div></li>
                        <li data-theme="orange"><div class="orange"></div></li>
                        <li data-theme="blush" class="active"><div class="blush"></div></li>
                    </ul>                    
                </div>
               
            </div>                
        </div>       
        </div>
        </aside>
<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">

    <div class="navbar-brand">
        <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>
        <a href="dashboard"><img src="/public/img/Gapa_new_1.png" width="25" alt="Aero"><span class="m-l-10">Admin Access</span></a>
    </div>
    <div class="navbar-right">
    <ul class="navbar-nav">
<li><a href="javascript:void(0);" class="js-right-sidebar" title="Setting"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a></li>
<li class="nav-item dropdown js-right-sidebar">
                               <br> <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                               <span style="font-size:20px;" class="zmdi zmdi-power"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
</ul>
</div>
    <div class="menu">
        <ul class="list">
            <li>
          
                <div class="user-info">
                @if(Auth::guard('admin')->check())
                    <a class="image" href="{{Auth::guard('admin')->user()->id}}editadmin"><img class="w50"  src="public/storage/admin_image/{{Auth::guard('admin')->user()->image}}"   alt="img" >
                    <div class="detail">
                        
                       
                   <h6> {{ Auth::guard('admin')->user()->name }}</h6></a>
                   
                    @endif
                                              
                    </div>
                </div>
            </li>
           
            <li><a href="/dashboard"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
            <li><a href="/settings"><i class="zmdi zmdi-settings"></i><span>Settings</span></a></li>
             <li><a href="/transactions"><i class="zmdi zmdi-balance-wallet"></i><span>Transactions</span></a></li>
            <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-apps"></i><span>Delivery Men</span></a>
                <ul class="ml-menu">
                    <li><a href="adddelivery">Add Delivery Man</a></li>
                    <li><a href="delivery-men">View Delivery Men</a></li>
                  
                </ul>
            </li>
            <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-shopping-cart"></i><span>Orders</span></a>
                <ul class="ml-menu">
                    <li><a href="pending">Pending Orders</a></li>
                    <li><a href="ongoing">Ongoing Orders</a></li>
                    <li><a href="delivered">Delivered Orders</a></li>
                    <li><a href="rejected">Rejected Orders</a></li>
 
                </ul>
            </li>
           
                        <!-- Authentication Links -->
                   
                    
           </ul>
           </div>

</aside>


</body>
</html>